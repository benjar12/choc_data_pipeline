package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait LookupGender extends HasSqlContext {
  sqlContext.read.json(ConfigurationSunriseChoc.hl7TableFolder + "/v2/0001.json")
    .registerTempTable("LOOKUP_GENDER")
}
