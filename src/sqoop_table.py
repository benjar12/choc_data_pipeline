
import luigi

#from luigi.contrib.external_program import ExternalProgramTask
from lib.external_program import ExternalProgramTask
from lib.task_with_complete_handler import TaskWithCompleteHandler

class SqoopTable(ExternalProgramTask):

    db = luigi.Parameter()
    schema = luigi.Parameter()
    table = luigi.Parameter()
    columns = luigi.Parameter()
    base_path = luigi.Parameter()
    java_types = luigi.Parameter()

    #print(self.schema)

    def program_args(self):
        target_dir = "{0}/{1}/{2}/{3}".format(self.base_path, self.db, self.schema, self.table)
        cmd = [
            "sqoop",
            "import",
            "-Doraoop.disabled=true",
            "--connect",
            "\"jdbc:sqlserver://choc-socpa-touchworks-sql.service.teal.consul:1433;databaseName=%s\"" % (self.db),
            "--username", "AMHS-REPORTING",
            "--password", "reports",
            "-m", "1",
            "--as-parquetfile",
            "--target-dir", target_dir,
            "--delete-target-dir",
            "--table", self.table,
            "--columns", "\"{0}\"".format(self.columns)
            ]

        if self.java_types != " ":
            cmd.append("--map-column-java")
            cmd.append(self.java_types)

        cmd.append("--")
        cmd.append("--schema")
        cmd.append(self.schema)

        return cmd

if __name__ == '__main__':
    # since we are setting MySecondTask to be the main task,
    # it will check for the requirements first, then run
    columns = "IB_Emergency_Contact_Info_ID,IB_Emergency_Contact_Info_TS,Emergency_Contact_External_ID,Emergency_Contact_Role,Emergency_Contact_First_Name,Emergency_Contact_Middle_Initial,Emergency_Contact_Last_Name,Emergency_Contact_Suffix,Emergency_Contact_Relationship,Emergency_Contact_Street1,Emergency_Contact_Street2,Emergency_Contact_City,Emergency_Contact_State,Emergency_Contact_Zip_Code,Emergency_Contact_Phone,Emergency_Contact_Phone_Ext,Emergency_Contact_Work_Phone,Emergency_Contact_Work_Phone_Ext,Emergency_Contact_Email_Address,Emergency_Contact_Marital_Status,Emergency_Contact_Sex,Emergency_Contact_SSN,Emergency_Contact_Date_Of_Birth,Emergency_Contact_Job_Title,Emergency_Contact_Employment_Status,Emergency_Contact_Emp_External_ID,Emergency_Contact_Emp_Name,Emergency_Contact_Emp_Contact_Name,Emergency_Contact_Emp_Street1,Emergency_Contact_Emp_Street2,Emergency_Contact_Emp_City,Emergency_Contact_Emp_State,Emergency_Contact_Emp_Zip_Code,Emergency_Contact_Emp_Phone,Emergency_Contact_Emp_Phone_Ext,Emergency_Contact_Comments,IB_Patient_Info_ID,Emergency_Contact_ID,Emergency_Contact_Address_ID,Invalid_Data,Import_Status,DateTime_Updated,Emergency_Contact_Cell_Phone,Emergency_Contact_Middle_Name,Emergency_Contact_Maiden_Name,Emergency_Contact_Relationship_ID,Emergency_Contact_Match_Factor,Min_Match_Factor,IB_TimeZone_Info_ID"

    luigi.run([
        "--local-scheduler",
        "--db", "EMR",
        "--schema", "dbo",
        "--table", "IB_Emergency_Contact_Info",
        "--columns", columns,
        "--base-path", "/tmp/hartos/sqoop/choc/sunrise/socpa/2017/02/07",
        "--java-types", " "],
        main_task_cls=SqoopTable)