#!/bin/bash

export HADOOP_USER_NAME=hdfs

export PROJECT_ROOT=/home/ben.jarman/choc_data_pipeline
export SRC_PATH=$PROJECT_ROOT/src

export PYTHONPATH=$PYTHONPATH:$SRC_PATH

export LUIGI_CONFIG_PATH=$PROJECT_ROOT/etc/luigi.cfg

cd $SRC_PATH

luigi --workers 4 --module run_and_clean RunAndClean
