package com.hart.hartos.etl.sunrise.tables

trait EncounterTables
  extends EmrHpsiteDemographics
  with EmrHpsiteEncounter
  with EmrHpsiteLocation
  with EmrHpsiteLocationRoom
  with EmrHpsiteSites {
}
