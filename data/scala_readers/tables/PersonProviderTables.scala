package com.hart.hartos.etl.sunrise.tables

trait PersonProviderTables
  extends EmrHpsiteDemographics
  with EmrHpsiteProviders {
}
