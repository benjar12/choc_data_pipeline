import luigi
import os
from sqoop_table import SqoopTable
from lib.task_with_complete_handler import TaskWithCompleteHandler

import pandas as pd

class SqoopRequiredTables(TaskWithCompleteHandler):

    is_done = False

    # This will look like
    # /hartos/sqoop/choc/sunrise/socpa/2017/02/07/
    base_path = luigi.Parameter()

    def to_sqoop_task(self, job):
        db = job[1]
        schema = job[2]
        table = job[3]
        columns = job[4]
        java_types = job[5]
        # This looks weird, but it takes care of nan
        if java_types != java_types:
            java_types = " "

        return SqoopTable(
            db=db,
            schema=schema,
            table=table,
            columns=columns,
            base_path=self.base_path,
            java_types=java_types)

    def requires(self):
        csv_path = os.path.abspath("../data/required_tables_with_columns.csv")
        df = pd.read_csv(csv_path)
        print(df.head())
        list = df.values.tolist()
        required_tasks = [self.to_sqoop_task(job) for job in list]
        print(len(required_tasks))
        return required_tasks


if __name__ == '__main__':
    luigi.run(["--local-scheduler", "--base-path", "/tmp/choc_test"], main_task_cls=SqoopRequiredTables)