package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait EmrHpsitePatientPhysicians extends HasSqlContext {
  sqlContext.read.parquet(ConfigurationSunriseChoc.inputFolder + "/EMR/HPSITE/PATIENT_PHYSICIANS")
    .registerTempTable("EMR_HPSITE_PATIENT_PHYSICIANS")
}
