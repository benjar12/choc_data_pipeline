import os

from luigi.contrib.spark import SparkSubmitTask
from lib.error_handler import ErrorHandler

class SparkSubmitSubTask(SparkSubmitTask, ErrorHandler):

    # All of these need to be overridden
    # by whatever subclasses this class
    class_name = None
    entry_class_base = None
    base_path = None
    output_folder = None
    port = None

    def spark_command(self):

        #conf_executor = "\"spark.executor.extraJavaOptions=-Dhdfs.input_folder={0} -Dhdfs.output_folder={1}\"" \
        #    .format(self.base_path, self.output_folder)

        conf_driver = {"spark.driver.extraJavaOptions":"-Dhdfs.input_folder={0} -Dhdfs.output_folder={1} -Dhdfs.user=hdfs -Dgolden_patient_ids=\"\"" \
            .format(self.base_path, self.output_folder),
                       "spark.ui.port": self.port
                       }

        #conf = [conf_driver, conf_executor]

        entry_class = self.entry_class_base.format(self.class_name)

        #print(conf)

        command = [self.spark_submit]
        command += self._text_arg('--master', self.master)
        command += self._text_arg('--deploy-mode', self.deploy_mode)
        command += self._text_arg('--name', self.name)
        command += self._text_arg('--class', entry_class)
        command += self._list_arg('--jars', self.jars)
        command += self._list_arg('--packages', self.packages)
        command += self._list_arg('--py-files', self.py_files)
        command += self._list_arg('--files', self.files)
        command += self._list_arg('--archives', self.archives)
        command += self._dict_arg('--conf', conf_driver)
        command += self._text_arg('--properties-file', self.properties_file)
        command += self._text_arg('--driver-memory', self.driver_memory)
        command += self._text_arg('--driver-java-options', self.driver_java_options)
        command += self._text_arg('--driver-library-path', self.driver_library_path)
        command += self._text_arg('--driver-class-path', self.driver_class_path)
        command += self._text_arg('--executor-memory', self.executor_memory)
        command += self._text_arg('--driver-cores', self.driver_cores)
        command += self._flag_arg('--supervise', self.supervise)
        command += self._text_arg('--total-executor-cores', self.total_executor_cores)
        command += self._text_arg('--executor-cores', self.executor_cores)
        command += self._text_arg('--queue', self.queue)
        command += self._text_arg('--num-executors', self.num_executors)
        return command