package com.hart.hartos.etl.sunrise

import java.sql.Date

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.ReferralRequestTables
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.Row
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.{ArrayType, IntegerType, StringType, StructField, StructType}
import shapeless.HNil

object ReferralRequestReader extends BaseSqlApp("ReferralRequestReader") with ExtraFunctions 
  with ReferralRequestTables with LazyLogging{
  logger.info(s"------------ReferralRequestReader v$version begins")
  val beg: Long = System.currentTimeMillis

  val timeRangeCondition1 =
    if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      ""
    else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
      s"AND r.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      s"AND r.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
    else
      s"AND r.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND r.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
  val goldenUserCondition =
    if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
      ""
    else {
      val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
      s"AND dm.DEM_EXTERNALID in ('$mrnLists')"
    }

  sqlContext.sql("""
      SELECT e.IMREDEMEC_CODE as PATIENT_ID, dx.DX_CODE as DiagnosisCodeID,
        CONCAT(dx.DX_TITLE, '\n', dx.DX_COMMENT) as DiagnosisDisplay,
        dx.TAG_SYSTEMDATE as DiagnosisDate
      FROM EMR_HPSITE_REFERRALS r
      JOIN EMR_HPSITE_DEMOGRAPHICS dm on dm.IMREDEM_CODE=r.IMREDEMEC_CODE
      JOIN EMR_HPSITE_ENCOUNTER e on e.IMREDEMEC_CODE=dm.IMREDEM_CODE
      LEFT JOIN EMR_HPSITE_PROVIDERS p on dm.DEM_PREFERREDPROVIDER=p.IMREPROV_CODE
      LEFT JOIN EMR_HPSITE_ENCOUNTER_ASSESSMENT asmt ON e.IMREENC_CODE = asmt.ENCOUNTER_ID
      JOIN EMR_HPSITE_DX dx ON asmt.DX_ID = dx.IMREDX_CODE
      WHERE dx.DX_CODE <> '' and dm.DEM_LASTNAME <> 'test' """ + goldenUserCondition + " " + timeRangeCondition1 + """
      ORDER BY e.IMREDEMEC_CODE, DiagnosisCodeID, DiagnosisDisplay, DiagnosisDate DESC
    """).registerTempTable("dfDiagnosis")

  val dfDiagnosisType = sqlContext.sql("""
      SELECT PATIENT_ID, DiagnosisCodeID,
        FIRST(DiagnosisDisplay) as DiagnosisDisplay,
        FIRST(DiagnosisDate) as DiagnosisDate
      FROM dfDiagnosis
      GROUP BY PATIENT_ID, DiagnosisCodeID
    """)
  val rddDiagnosisByPatient = dfDiagnosisType.map(x=>(x.getAs[String]("PATIENT_ID"),
    (x.getAs[String]("DiagnosisCodeID"), x.getAs[String]("DiagnosisDisplay"), x.getAs[String]("DiagnosisDate"))))
    .groupByKey
    .map(x=>Row(x._1, x._2))

  val schema = StructType(Seq(
    StructField("PATIENT_ID", IntegerType, nullable=false),
    StructField("Diagnosis", ArrayType(StructType(Seq(
      StructField("DiagnosisCodeID", StringType),
      StructField("DiagnosisDisplay", StringType),
      StructField("DiagnosisDate", StringType)
    )))))
  )
  sqlContext.createDataFrame(rddDiagnosisByPatient, schema).registerTempTable("dfDiagnosisByPatient")

  val timeRangeCondition2 =
    if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      ""
    else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
      s"WHERE r.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      s"WHERE r.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
    else
      s"WHERE r.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND r.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"

  val query = s"""
    SELECT distinct
      0 as DeleteInd,
      0 as TenantID,
      r.IMREREF_CODE as ReferralRequestID,
      e.ENC_CHARTPULLTIME as Version,
      'v$version' as CodeVersion,
      dm.IMREDEM_CODE as PersonID,
      dm.DEM_EXTERNALID as MRN,
      '2.16.840.1.113883.3.4173' as MRNAssigningAuthority,
      dm.DEM_SSNUM as SSN,
      dm.DEM_LASTNAME as LastName,
      dm.DEM_FIRSTNAME as FirstName,
      dm.DEM_MIDDLEINITIAL as MiddleName,
      dm.DEM_DATEOFBIRTH as BirthDate,
      case when dm.DEM_SEX = 1 then 'F' when dm.DEM_SEX = 2 then 'M' else 'O' end as GenderCodeID,
      '2.16.840.1.133883.18.2' as GenderCodeSystemID,
      case when dm.DEM_SEX = 1 then 'Female' when dm.DEM_SEX = 2 then 'Male' else 'Other' end as GenderDisplay,
      a.ADDR_ADDRESS as StreetAddress1,
      '' as StreetAddress2,
      a.ADDR_CITY as City,
      TRIM(a.ADDR_STATE) as LocalityCodeID,
      '' as LocalityCodeSystemID,
      s.STATE_NAME as LocalityDisplay,
      a.ADDR_ZIP as PostalCode,
      a.ADDR_PHONE as PrimaryPhoneNumber,
      '' as EstimatedDateOfBirth,
      r.REF_ORDERDATE as SentDate,
      '' as ReferralReasonCodeID,
      '' as ReferralReasonCodeSystemID,
      r.REF_CONSULTATIONREASON as ReferralReasonDisplay,
      d.Diagnosis as Diagnosis,
      p.IMREPROV_CODE as RequestingProviderID,
      p.PROV_ID as RequestingProviderIdentifier,
      p.PROV_TYPE as RequestingProviderIdentifierType,
      p.PROV_LASTNAME as RequestingProviderLastName,
      p.PROV_FIRSTNAME as RequestingProviderFirstName,
      p.PROV_MIDDLEINITIAL as RequestingProviderMiddleName,
      TRIM(CONCAT(p.PROV_LASTNAME, ', ', p.PROV_FIRSTNAME, ' ', p.PROV_MIDDLEINITIAL)) as RequestingProviderFullName
    FROM EMR_HPSITE_REFERRALS r
    JOIN EMR_HPSITE_DEMOGRAPHICS dm on dm.IMREDEM_CODE=r.IMREDEMEC_CODE
    JOIN dfDiagnosisByPatient d on dm.IMREDEM_CODE=d.PATIENT_ID
    JOIN EMR_HPSITE_ENCOUNTER e on e.IMREDEMEC_CODE=d.PATIENT_ID
    LEFT JOIN EMR_HPSITE_PROVIDERS p on r.REF_CODE=p.IMREPROV_CODE
    LEFT JOIN EMR_HPSITE_ADDRESSES a on a.IMREADDR_CODE=d.PATIENT_ID
    LEFT JOIN EMR_hpsystem_STATES s on TRIM(s.STATE_CODE)=TRIM(a.ADDR_STATE)
    WHERE dm.DEM_LASTNAME <> 'test' $goldenUserCondition $timeRangeCondition2
    """
  val output = sqlContext.sql(query)
  val bodyTxt = output.map(x=>{
    val diags = x.getAs[Seq[GenericRowWithSchema]]("Diagnosis")
      .map(y => (y.getAs[String](0),y.getAs[String](1),y.getAs[String](2))).toArray
    val diagSize = diags.length
    val principalDiagnosisCodeID = if (diagSize > 0) diags(0)._1 else ""
    val principalDiagnosisDisplay = if (diagSize > 0) diags(0)._2 else ""
    val principalDiagnosisDate = if (diagSize > 0) diags(0)._3 else ""
    val otherDiagnosisCodeID1 = if (diagSize > 1) diags(1)._1 else ""
    val otherDiagnosisDisplay1 = if (diagSize > 1) diags(1)._2 else ""
    val otherDiagnosisDate1 = if (diagSize > 1) diags(1)._3 else ""
    val otherDiagnosisCodeID2 = if (diagSize > 2) diags(2)._1 else ""
    val otherDiagnosisDisplay2 = if (diagSize > 2) diags(2)._2 else ""
    val otherDiagnosisDate2 = if (diagSize > 2) diags(2)._3 else ""
    val otherDiagnosisCodeID3 = if (diagSize > 3) diags(3)._1 else ""
    val otherDiagnosisDisplay3 = if (diagSize > 3) diags(3)._2 else ""
    val otherDiagnosisDate3 = if (diagSize > 3) diags(3)._3 else ""
    val otherDiagnosisCodeID4 = if (diagSize > 4) diags(4)._1 else ""
    val otherDiagnosisDisplay4 = if (diagSize > 4) diags(4)._2 else ""
    val otherDiagnosisDate4 = if (diagSize > 4) diags(4)._3 else ""
    val otherDiagnosisCodeID5 = if (diagSize > 5) diags(5)._1 else ""
    val otherDiagnosisDisplay5 = if (diagSize > 5) diags(5)._2 else ""
    val otherDiagnosisDate5 = if (diagSize > 5) diags(5)._3 else ""
    val otherDiagnosisCodeID6 = if (diagSize > 6) diags(6)._1 else ""
    val otherDiagnosisDisplay6 = if (diagSize > 6) diags(6)._2 else ""
    val otherDiagnosisDate6 = if (diagSize > 6) diags(6)._3 else ""
    val otherDiagnosisCodeID7 = if (diagSize > 7) diags(7)._1 else ""
    val otherDiagnosisDisplay7 = if (diagSize > 7) diags(7)._2 else ""
    val otherDiagnosisDate7 = if (diagSize > 7) diags(7)._3 else ""
    val otherDiagnosisCodeID8 = if (diagSize > 8) diags(8)._1 else ""
    val otherDiagnosisDisplay8 = if (diagSize > 8) diags(8)._2 else ""
    val otherDiagnosisDate8 = if (diagSize > 8) diags(8)._3 else ""
    val otherDiagnosisCodeID9 = if (diagSize > 9) diags(9)._1 else ""
    val otherDiagnosisDisplay9 = if (diagSize > 9) diags(9)._2 else ""
    val otherDiagnosisDate9 = if (diagSize > 9) diags(9)._3 else ""
    val otherDiagnosisCodeID10 = if (diagSize > 10) diags(10)._1 else ""
    val otherDiagnosisDisplay10 = if (diagSize > 10) diags(10)._2 else ""
    val otherDiagnosisDate10 = if (diagSize > 10) diags(10)._3 else ""
    val otherDiagnosisCodeID11 = if (diagSize > 11) diags(11)._1 else ""
    val otherDiagnosisDisplay11 = if (diagSize > 11) diags(11)._2 else ""
    val otherDiagnosisDate11 = if (diagSize > 11) diags(11)._3 else ""
    val otherDiagnosisCodeID12 = if (diagSize > 12) diags(12)._1 else ""
    val otherDiagnosisDisplay12 = if (diagSize > 12) diags(12)._2 else ""
    val otherDiagnosisDate12 = if (diagSize > 12) diags(12)._3 else ""
    (x.getAs[Integer]("DeleteInd") ::
      x.getAs[Integer]("TenantID") ::
      x.getAs[Integer]("ReferralRequestID") ::
      x.getAs[String]("Version") + "_" + x.getAs[String]("CodeVersion") ::
      x.getAs[Integer]("PersonID") ::
      x.getAs[String]("MRN") ::
      x.getAs[String]("MRNAssigningAuthority") ::
      x.getAs[String]("SSN") ::
      x.getAs[String]("LastName") ::
      x.getAs[String]("FirstName") ::
      x.getAs[String]("MiddleName") ::
      x.getAs[Date]("BirthDate") ::
      x.getAs[String]("GenderCodeID") ::
      x.getAs[String]("GenderCodeSystemID") ::
      x.getAs[String]("GenderDisplay") ::
      x.getAs[String]("StreetAddress1") ::
      x.getAs[String]("StreetAddress2") ::
      x.getAs[String]("City") ::
      x.getAs[String]("LocalityCodeID") ::
      x.getAs[String]("LocalityCodeSystemID") ::
      x.getAs[String]("LocalityDisplay") ::
      x.getAs[String]("PostalCode") ::
      x.getAs[String]("PrimaryPhoneNumber") ::
      principalDiagnosisCodeID ::
      "" :: //PrincipalDiagnosisCodeSystemID
      principalDiagnosisDisplay ::
      principalDiagnosisDate ::
      principalDiagnosisCodeID :: //AdmittingDiagnosisCodeID
      otherDiagnosisCodeID1 ::
      "" :: //OtherDiagnosisCodeSystemID1
      otherDiagnosisDisplay1 ::
      otherDiagnosisDate1 ::
      otherDiagnosisCodeID2 ::
      "" :: //OtherDiagnosisCodeSystemID2
      otherDiagnosisDisplay2 ::
      otherDiagnosisDate2 ::
      otherDiagnosisCodeID3 ::
      "" :: //OtherDiagnosisCodeSystemID3
      otherDiagnosisDisplay3 ::
      otherDiagnosisDate3 ::
      otherDiagnosisCodeID4 ::
      "" :: //OtherDiagnosisCodeSystemID4
      otherDiagnosisDisplay4 ::
      otherDiagnosisDate4 ::
      otherDiagnosisCodeID5 ::
      "" :: //OtherDiagnosisCodeSystemID5
      otherDiagnosisDisplay5 ::
      otherDiagnosisDate5 ::
      otherDiagnosisCodeID6 ::
      "" :: //OtherDiagnosisCodeSystemID6
      otherDiagnosisDisplay6 ::
      otherDiagnosisDate6 ::
      otherDiagnosisCodeID7 ::
      "" :: //OtherDiagnosisCodeSystemID7
      otherDiagnosisDisplay7 ::
      otherDiagnosisDate7 ::
      otherDiagnosisCodeID8 ::
      "" :: //OtherDiagnosisCodeSystemID8
      otherDiagnosisDisplay8 ::
      otherDiagnosisDate8 ::
      otherDiagnosisCodeID9 ::
      "" :: //OtherDiagnosisCodeSystemID9
      otherDiagnosisDisplay9 ::
      otherDiagnosisDate9 ::
      otherDiagnosisCodeID10 ::
      "" :: //OtherDiagnosisCodeSystemID10
      otherDiagnosisDisplay10 ::
      otherDiagnosisDate10 ::
      otherDiagnosisCodeID11 ::
      "" :: //OtherDiagnosisCodeSystemID11
      otherDiagnosisDisplay11 ::
      otherDiagnosisDate11 ::
      otherDiagnosisCodeID12 ::
      "" :: //OtherDiagnosisCodeSystemID12
      otherDiagnosisDisplay12 ::
      otherDiagnosisDate12 ::
      x.getAs[String]("EstimatedDateOfBirth") ::
      x.getAs[String]("SentDate") ::
      x.getAs[String]("ReferralReasonCodeID") ::
      x.getAs[String]("ReferralReasonCodeSystemID") ::
      x.getAs[String]("ReferralReasonDisplay") ::
      x.getAs[Integer]("RequestingProviderID") ::
      x.getAs[String]("RequestingProviderIdentifier") ::
      x.getAs[Integer]("RequestingProviderIdentifierType") ::
      x.getAs[String]("RequestingProviderLastName") ::
      x.getAs[String]("RequestingProviderFirstName") ::
      x.getAs[String]("RequestingProviderMiddleName") ::
      x.getAs[String]("RequestingProviderFullName") :: HNil).toPipeCsv})
  val header = Array[String]("DeleteInd|TenantID|ReferralRequestID|Version|PersonID|MRN|MRNAssigningAuthority|SSN|LastName|FirstName|MiddleName|BirthDate|GenderCodeID|GenderCodeSystemID|GenderDisplay|StreetAddress1|StreetAddress2|City|LocalityCodeID|LocalityCodeSystemID|LocalityDisplay|PostalCode|PrimaryPhoneNumber|principalDiagnosisCodeID|PrincipalDiagnosisCodeSystemID|principalDiagnosisDisplay|principalDiagnosisDate|principalDiagnosisCodeID|otherDiagnosisCodeID1|OtherDiagnosisCodeSystemID1|otherDiagnosisDisplay1|otherDiagnosisDate1|otherDiagnosisCodeID2|OtherDiagnosisCodeSystemID2|otherDiagnosisDisplay2|otherDiagnosisDate2|otherDiagnosisCodeID3|OtherDiagnosisCodeSystemID3|otherDiagnosisDisplay3|otherDiagnosisDate3|otherDiagnosisCodeID4|OtherDiagnosisCodeSystemID4|otherDiagnosisDisplay4|otherDiagnosisDate4|otherDiagnosisCodeID5|OtherDiagnosisCodeSystemID5|otherDiagnosisDisplay5|otherDiagnosisDate5|otherDiagnosisCodeID6|OtherDiagnosisCodeSystemID6|otherDiagnosisDisplay6|otherDiagnosisDate6|otherDiagnosisCodeID7|OtherDiagnosisCodeSystemID7|otherDiagnosisDisplay7|otherDiagnosisDate7|otherDiagnosisCodeID8|OtherDiagnosisCodeSystemID8|otherDiagnosisDisplay8|otherDiagnosisDate8|otherDiagnosisCodeID9|OtherDiagnosisCodeSystemID9|otherDiagnosisDisplay9|otherDiagnosisDate9|otherDiagnosisCodeID10|OtherDiagnosisCodeSystemID10|otherDiagnosisDisplay10|otherDiagnosisDate10|otherDiagnosisCodeID11|OtherDiagnosisCodeSystemID11|otherDiagnosisDisplay11|otherDiagnosisDate11|otherDiagnosisCodeID12|OtherDiagnosisCodeSystemID12|otherDiagnosisDisplay12|otherDiagnosisDate12|EstimatedDateOfBirth|SentDate|ReferralReasonCodeID|ReferralReasonCodeSystemID|ReferralReasonDisplay|RequestingProviderID|RequestingProviderIdentifier|RequestingProviderIdentifierType|RequestingProviderLastName|RequestingProviderFirstName|RequestingProviderMiddleName|RequestingProviderFullName")
  val headerRDD = sc.parallelize(header)
  val outputTxt = headerRDD.union(bodyTxt)
  val destiPath = s"${ConfigurationSunriseChoc.outputFolder}/referral_request"
  outputTxt.coalesce(1).saveAsTextFile(destiPath)
  val end: Long = System.currentTimeMillis
  val diff = end - beg
  logger.info(s"------------It took $diff ms to write Referral Request to $destiPath")
}
