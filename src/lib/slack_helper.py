import requests
import json

url = "https://hooks.slack.com/services/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"

def log_error(error):
    dist_data = {"text": error}
    data = json.dumps(dist_data)
    requests.post(url, data)

if __name__ == '__main__':
    log_error("This is a test from python :) :troll:")