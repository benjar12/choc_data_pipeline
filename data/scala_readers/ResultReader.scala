package com.hart.hartos.etl.sunrise

import java.util.Date

import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.hart.hartos.etl.sunrise.tables.ResultTables
import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.util.FileIOSupport
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SQLContext
import shapeless.HNil

object ResultReader extends BaseSqlApp("ResultReader") with ExtraFunctions with ResultTables {
  ResultReaderProcess.execute(sqlContext, version)

  // Shutdown app
  sc.stop()
}

object ResultReaderProcess extends FileIOSupport with LazyLogging {
  def execute(sqlContext: SQLContext, version: String): Unit = {
    logger.info(s"------------ResultReader v$version begins at ${new Date}")
    val beg: Long = System.currentTimeMillis

    val timeRangeCondition =
      if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        ""
      else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND l.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
      else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND l.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
      else
        s"AND l.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND l.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    val goldenUserCondition =
      if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
        ""
      else {
        val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
        s"AND d.DEM_EXTERNALID in ('$mrnLists')"
      }
    val query = s"""
    SELECT distinct
      0 as DeleteInd,
      0 as TenantID,
      l.RESULT_ID as ResultID,
      e.ENC_CHARTPULLTIME as Version,
      'v$version' as CodeVersion,
      d.IMREDEM_CODE as PersonID,
      e.IMREENC_CODE as EncounterID,
      l.RESULT_DATA_TYPE as ResultTypeCodeID,
      '' as ResultTypeCodeSystemID,
      l.RESULT_NAME as ResultTypeDisplay,
      '' as UnitOfMeasureCodeID,
      '' as UnitOfMeasureCodeSystemID,
      l.RESULT_UNITS as UnitOfMeasureDisplay,
      l.RESULT_VALUE as ResultValueNumber,
      TRIM(CONCAT(l.RESULT_VALUE, l.RESULT_UNITS)) as ResultValueText,
      l.RESULT_NORMAL_RANGE as ReferenceRangeText,
      '' as InterpretationCodeID,
      '' as InterpretationCodeSystemID,
      '' as InterpretationDisplay,
      '' as ResultDate,
      '' as LastClinicalSignificntUpdateDate,
      l.RESULT_COMMENTS as Comment,
      l.RESULT_REVIEWEDSTATUS as StatusCodeID,
      '' as StatusCodeSystemID,
      '' as StatusDisplay,
      '' as SpecimenTypeCodeID,
      '' as SpecimenTypeCodeSystemID,
      '' as SpecimenTypeDisplay,
      '' as MeasurementMethodCodeID,
      '' as MeasurementMethodCodeSystemID,
      '' as MeasurementMethodDisplay,
      '' as ResultValueCodeID,
      '' as ResultValueCodeSystemID,
      '' as ResultValueCodeDisplay,
      '' as Context,
      l.RESULT_COMPLETIONDATE as ResultValueDate,
      '' as Accession,
      '' as RecorderType
    FROM EMR_HPSITE_LAB_RESULT l
    JOIN EMR_HPSITE_DEMOGRAPHICS d on l.IMREDEM_CODE=d.IMREDEM_CODE
    JOIN EMR_HPSITE_ENCOUNTER e on e.IMREDEMEC_CODE=d.IMREDEM_CODE
    WHERE d.DEM_LASTNAME <> 'test' $goldenUserCondition $timeRangeCondition
    """
    val output = sqlContext.sql(query)
    val bodyTxt = output.map(x=>
      (x.getAs[Integer]("DeleteInd") ::
        x.getAs[Integer]("TenantID") ::
        x.getAs[Integer]("ResultID") ::
        x.getAs[String]("Version") + "_" + x.getAs[String]("CodeVersion") ::
        x.getAs[Integer]("PersonID") ::
        x.getAs[Integer]("EncounterID") ::
        x.getAs[String]("ResultTypeCodeID") ::
        x.getAs[String]("ResultTypeCodeSystemID") ::
        x.getAs[String]("ResultTypeDisplay") ::
        x.getAs[String]("UnitOfMeasureCodeID") ::
        x.getAs[String]("UnitOfMeasureCodeSystemID") ::
        x.getAs[String]("UnitOfMeasureDisplay") ::
        x.getAs[String]("ResultValueNumber") ::
        x.getAs[String]("ResultValueText") ::
        x.getAs[String]("ReferenceRangeText") ::
        x.getAs[String]("InterpretationCodeID") ::
        x.getAs[String]("InterpretationCodeSystemID") ::
        x.getAs[String]("InterpretationDisplay") ::
        x.getAs[String]("ResultDate") ::
        x.getAs[String]("LastClinicalSignificntUpdateDate") ::
        x.getAs[String]("Comment") ::
        x.getAs[Integer]("StatusCodeID") ::
        x.getAs[String]("StatusCodeSystemID") ::
        x.getAs[String]("StatusDisplay") ::
        x.getAs[String]("SpecimenTypeCodeID") ::
        x.getAs[String]("SpecimenTypeCodeSystemID") ::
        x.getAs[String]("SpecimenTypeDisplay") ::
        x.getAs[String]("MeasurementMethodCodeID") ::
        x.getAs[String]("MeasurementMethodCodeSystemID") ::
        x.getAs[String]("MeasurementMethodDisplay") ::
        x.getAs[String]("ResultValueCodeID") ::
        x.getAs[String]("ResultValueCodeSystemID") ::
        x.getAs[String]("ResultValueCodeDisplay") ::
        x.getAs[String]("Context") ::
        x.getAs[String]("ResultValueDate") ::
        x.getAs[String]("Accession") ::
        x.getAs[String]("RecorderType") :: HNil).toPipeCsv)

    val destPath = s"${ConfigurationSunriseChoc.outputFolder}/result"
    val headers = List("DeleteInd","TenantID","ResultID","Version","PersonID","EncounterID","ResultTypeCodeID",
      "ResultTypeCodeSystemID","ResultTypeDisplay","UnitOfMeasureCodeID","UnitOfMeasureCodeSystemID","UnitOfMeasureDisplay",
      "ResultValueNumber","ResultValueText","ReferenceRangeText","InterpretationCodeID","InterpretationCodeSystemID",
      "InterpretationDisplay","ResultDate","LastClinicalSignificntUpdateDate","Comment","StatusCodeID","StatusCodeSystemID",
      "StatusDisplay","SpecimenTypeCodeID","SpecimenTypeCodeSystemID","SpecimenTypeDisplay","MeasurementMethodCodeID",
      "MeasurementMethodCodeSystemID","MeasurementMethodDisplay","ResultValueCodeID","ResultValueCodeSystemID",
      "ResultValueCodeDisplay","Context","ResultValueDate","Accession","RecorderType")

    saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destPath)

    val end: Long = System.currentTimeMillis
    val diff = end - beg
    logger.info(s"------------It took $diff ms to write Result to $destPath. Finished at ${new Date()}")
  }
}
