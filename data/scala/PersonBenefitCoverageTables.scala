package com.hart.hartos.etl.sunrise.tables

trait PersonBenefitCoverageTables
  extends EmrHpsiteDemographics
  with EmrHpsiteInsurances
  with EmrHpsiteInsurancesCarriers {
}
