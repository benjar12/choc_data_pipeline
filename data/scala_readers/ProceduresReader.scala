package com.hart.hartos.etl.sunrise

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.ProceduresTables
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import shapeless.HNil

object ProceduresReader extends BaseSqlApp("ProceduresReader") with ExtraFunctions 
  with ProceduresTables with LazyLogging{
  logger.info(s"------------ProceduresReader v$version begins")
  val beg: Long = System.currentTimeMillis

  val timeRangeCondition =
    if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      ""
    else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
      s"AND p.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      s"AND p.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
    else
      s"AND p.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND p.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
  val goldenUserCondition =
    if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
      ""
    else {
      val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
      s"AND demo.DEM_EXTERNALID in ('$mrnLists')"
    }
  val query = s"""
    SELECT distinct
      0 as DeleteInd,
      0 as TenantID,
      p.IMREPROC_CODE as ProcedureID,
      enc.ENC_CHARTPULLTIME as Version,
      'v$version' as CodeVersion,
      demo.IMREDEM_CODE as PersonID,
      enc.IMREENC_CODE as EncounterID,
      p.PROC_CODE as ProcedureCodeID,
      p.PROC_SOURCE as ProcedureCodeSystemID,
      p.PROC_TITLE as ProcedureDisplay,
      p.PROC_ORDERDATE as ProcedureDate,
      prov.PROV_ID as ProviderID,
      case when prov.NPI <> '' then prov.NPI when prov.PROV_UPIN <> '' then prov.PROV_UPIN else prov.PROV_DEAID end as ProviderIdentifier,
      case when prov.NPI <> '' then 'NPI' when prov.PROV_UPIN <> '' then 'UPIN' else 'DEA' end as ProviderIdentifierType,
      prov.PROV_LASTNAME as ProviderLastName,
      prov.PROV_FIRSTNAME as ProviderFirstName,
      prov.PROV_MIDDLEINITIAL as ProviderMiddlename,
      TRIM(CONCAT(prov.PROV_LASTNAME, ', ', prov.PROV_FIRSTNAME, ' ', prov.PROV_MIDDLEINITIAL)) as ProviderFullName,
      'OP' as ProviderRoleCodeID,
      '2.16.840.1.133883.18.283' as ProviderRoleCodeSystemID,
      'Ordering Provider' as ProviderRoleDisplay,
      p.PROC_COMMENT as Comment,
      '' as BillingRank
    FROM EMR_HPSITE_PROCEDURES p
    JOIN EMR_HPSITE_DEMOGRAPHICS demo on demo.IMREDEM_CODE = p.IMREDEMEC_CODE
    JOIN EMR_HPSITE_ENCOUNTER enc on enc.IMREDEMEC_CODE = demo.IMREDEM_CODE
    LEFT JOIN EMR_HPSITE_PROVIDERS prov on prov.IMREPROV_CODE = p.PROC_CONTACTPROVCODE
    WHERE demo.DEM_LASTNAME <> 'test' $goldenUserCondition $timeRangeCondition
  """
  val output = sqlContext.sql(query)
  val bodyTxt = output.map(x=>
      (x.getAs[Integer]("DeleteInd") ::
        x.getAs[Integer]("TenantID") ::
        x.getAs[Integer]("ProcedureID") ::
        x.getAs[String]("Version") + "_" + x.getAs[String]("CodeVersion") ::
        x.getAs[Integer]("PersonID") ::
        x.getAs[Integer]("EncounterID") ::
        x.getAs[String]("ProcedureCodeID") ::
        x.getAs[String]("ProcedureCodeSystemID") ::
        x.getAs[String]("ProcedureDisplay") ::
        x.getAs[String]("ProcedureDate") ::
        x.getAs[String]("ProviderID") ::
        x.getAs[String]("ProviderIdentifier") ::
        x.getAs[String]("ProviderIdentifierType") ::
        x.getAs[String]("ProviderLastName") ::
        x.getAs[String]("ProviderFirstName") ::
        x.getAs[String]("ProviderMiddlename") ::
        x.getAs[String]("ProviderFullName") ::
        x.getAs[String]("ProviderRoleCodeID") ::
        x.getAs[String]("ProviderRoleCodeSystemID") ::
        x.getAs[String]("ProviderRoleDisplay") ::
        x.getAs[String]("Comment") ::
        x.getAs[String]("BillingRank") :: HNil).toPipeCsv)
  val header = Array[String]("DeleteInd|TenantID|ProcedureID|Version|PersonID|EncounterID|ProcedureCodeID|ProcedureCodeSystemID|ProcedureDisplay|ProcedureDate|ProviderID|ProviderIdentifier|ProviderIdentifierType|ProviderLastName|ProviderFirstName|ProviderMiddlename|ProviderFullName|ProviderRoleCodeID|ProviderRoleCodeSystemID|ProviderRoleDisplay|Comment|BillingRank")
  val headerRDD = sc.parallelize(header)
  val outputTxt = headerRDD.union(bodyTxt)
  val destiPath = s"${ConfigurationSunriseChoc.outputFolder}/procedures"
  outputTxt.coalesce(1).saveAsTextFile(destiPath)
  val end: Long = System.currentTimeMillis
  val diff = end - beg
  logger.info(s"------------It took $diff ms to write Procedures to $destiPath")
}
