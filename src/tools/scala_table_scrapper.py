import re
import os
from pathlib import Path

import pandas as pd

table_regex = re.compile(r"""\+.+?\"(\/.+?)\"""")
no_json = re.compile(r".json")

path = os.path.abspath("../../data/scala/")

files = os.listdir(path)

files_content = [Path("%s/%s" % (path, x)).read_text() for x in files]

def find_table_name(content):
    matches = re.search(table_regex, content)
    if matches != None:
        match = matches.group(1)
        return match

table_paths = [find_table_name(x) for x in files_content]

cleaned = [x for x in table_paths if x != None and re.search(no_json, x) == None]

split = [x[1:].split("/") for x in cleaned]
headers = ["db", "schema", "table"]

df = pd.DataFrame(split, columns=headers)

csv_path = os.path.abspath("../../data/required_tables.csv")

df.to_csv(csv_path)