package com.hart.hartos.etl.sunrise

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.ProblemTables
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.util.{FileIOSupport, HdfsUtils, SmartQueryExecutor}
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import shapeless.HNil

object ProblemReader extends BaseSqlApp("ProblemReader") with ExtraFunctions with ProblemTables with FileIOSupport with LazyLogging {
  logger.info(s"------------ProblemReader v$version begins")
  val beg: Long = System.currentTimeMillis
  val smartQueryExecutor = new SmartQueryExecutor(sqlContext, "choc_problem")
  smartQueryExecutor.begin()

  val timeRangeCondition =
    if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      ""
    else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
      s"WHERE TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      s"WHERE TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
    else
      s"WHERE TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
  val goldenUserCondition =
    if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
      ""
    else {
      val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
      s"AND demo.DEM_EXTERNALID in ('$mrnLists')"
    }

  smartQueryExecutor.smartExecute(s"""
      select IMREDX_CODE, TAG_REVISION, IMREDEMEC_CODE, IMRECONTACT_CODE, TERM_UID, DX_ACTIVITY, DX_MODIFIER,
        DX_SIGNIFICANCE, ISPROMOTED_FLAG, DX_COMMENT,
        DX_DESCRIPTION, DX_TITLE, DX_ONSETDATE, DX_CATEGORY, TAG_SYSTEMDATE, DX_INTENSITY, DX_ACUITY, DX_STABILITY
      from EMR_HPSITE_DX
      $timeRangeCondition
      order by IMREDX_CODE, TAG_REVISION DESC
    """,
    "dxTmp")

  smartQueryExecutor.smartExecute(s"""
      select
        IMREDX_CODE,
        first(TAG_REVISION) as TAG_REVISION,
        first(IMREDEMEC_CODE) as IMREDEMEC_CODE,
        first(IMRECONTACT_CODE) as IMRECONTACT_CODE,
        first(TERM_UID) as TERM_UID,
        first(DX_COMMENT) as COMMENTS,
        first(ISPROMOTED_FLAG) as ISPROMOTED_FLAG,
        first(DX_ACTIVITY) as ACTIVITY_CODE,
        first(DX_MODIFIER) as MODIFIER_CODE,
        first(DX_SIGNIFICANCE) as SIGNIFICANCE_CODE,
        first(DX_DESCRIPTION) as DESCRIPTION,
        first(DX_INTENSITY) as INTENSITY_CODE,
        first(DX_TITLE) as TITLE,
        first(DX_ACUITY) as ACUITY_CODE,
        first(DX_STABILITY) as STABILITY_CODE,
        first(DX_ONSETDATE) as ONSET_DATE,
        first(DX_CATEGORY) as CATEGORY_CODE,
        first(TAG_SYSTEMDATE) as TAG_SYSTEMDATE
      from dxTmp
      group by IMREDX_CODE
    """,
    "dfDx")

  val dfDxProblem = smartQueryExecutor.smartExecute(s"""
      select
        0 as DeleteInd,
        0 as TenantID,
        enc.ENC_CHARTPULLTIME as Version,
        'v$version' as CodeVersion,
        dx.IMREDX_CODE as ProblemID,
        '' as ProblemCodeID,
        '' as ProblemCodeSystemID,
        coalesce(dx.DESCRIPTION, dx.TITLE) as ProblemDisplay,
        dx.COMMENTS as Comment,
        contact.IMRECONTACT_CODE,
        contact.IMREENC_CODE,
        termAtt.TERM_DESCRIPTION,
        dx.ONSET_DATE as OnsetDate,
        dictMaster.DICT_DESCRIPTION as Category,
        dx.CATEGORY_CODE as StatusCodeID,
        '' as StatusCodeSystemID,
        dictMaster2.DICT_DESCRIPTION as StatusDisplay,
        dictMaster3.DICT_DESCRIPTION as Modifier,
        dictMaster4.DICT_DESCRIPTION as Significant,
        dictMaster5.DICT_DESCRIPTION as Intensity,
        dictMaster6.DICT_DESCRIPTION as Acuity,
        dictMaster7.DICT_DESCRIPTION as Stability,
        enc.IMREENC_CODE as EncounterID,
        dx.IMREDEMEC_CODE as PersonID,
        dx.TAG_SYSTEMDATE as StatusChangeDate,
        prov1.PROV_ID as ProviderID,
        prov1.PROV_ID as ProviderIdentifier,
        prov1.PROV_TYPE as ProviderIdentifierType,
        prov1.PROV_LASTNAME as ProviderLastName,
        prov1.PROV_FIRSTNAME as ProviderFirstname,
        prov1.PROV_MIDDLEINITIAL as ProviderMiddleName,
        TRIM(CONCAT(prov1.PROV_LASTNAME, ', ', prov1.PROV_FIRSTNAME, ' ', prov1.PROV_MIDDLEINITIAL)) as ProviderFullName
      from dfDx dx
      join EMR_HPSITE_CONTACT contact on dx.IMRECONTACT_CODE = contact.IMRECONTACT_CODE
      join EMR_HPSITE_ENCOUNTER enc on enc.IMREENC_CODE = contact.IMREENC_CODE
      LEFT JOIN EMR_HPSITE_PROVIDERS prov1 on contact.IMREPROV_CODE = prov1.IMREPROV_CODE
      left join EMR_HPSITE_TERMINOLOGY_ATTRIBUTES termAtt on termAtt.TERM_UID = dx.TERM_UID
      LEFT JOIN EMR_hpsystem_DICTIONARIES_MASTER dictMaster on dx.CATEGORY_CODE = dictMaster.DICT_CODE and dictMaster.DICT_TYPE = 'PREFTYPE'
      LEFT JOIN EMR_hpsystem_DICTIONARIES_MASTER dictMaster2 on dx.ACTIVITY_CODE = dictMaster2.DICT_CODE and dictMaster2.DICT_TYPE = 'DXACT'
      LEFT JOIN EMR_hpsystem_DICTIONARIES_MASTER dictMaster3 on dx.MODIFIER_CODE = dictMaster3.DICT_CODE and dictMaster3.DICT_TYPE = 'DXMOD'
      LEFT JOIN EMR_hpsystem_DICTIONARIES_MASTER dictMaster4 on dx.SIGNIFICANCE_CODE = dictMaster4.DICT_CODE and dictMaster4.DICT_TYPE = 'DXSIG'
      LEFT JOIN EMR_hpsystem_DICTIONARIES_MASTER dictMaster5 on dx.INTENSITY_CODE = dictMaster5.DICT_CODE and dictMaster5.DICT_TYPE = 'DXINT'
      LEFT JOIN EMR_hpsystem_DICTIONARIES_MASTER dictMaster6 on dx.ACUITY_CODE = dictMaster6.DICT_CODE and dictMaster6.DICT_TYPE = 'DXACU'
      LEFT JOIN EMR_hpsystem_DICTIONARIES_MASTER dictMaster7 on dx.STABILITY_CODE = dictMaster7.DICT_CODE and dictMaster7.DICT_TYPE = 'DXSTA'
      where dx.ISPROMOTED_FLAG = 1
        and dx.ACTIVITY_CODE not in (8,9)
        and dx.CATEGORY_CODE in (3, 24, 21)
    """,
    "dfDxProblem")

  smartQueryExecutor.smartExecute(s"""
      select HXDIAGNOSIS_ID, TAG_REVISION, DESCRIPTION, TITLE, ONSET_DATE, TERM_UID, DIAGNOSIS_ID, COMMENTS, PATIENT_ID,
        CATEGORY_CODE, ACTIVITY_CODE, MODIFIER_CODE, SIGNIFICANCE_CODE, INTENSITY_CODE, CONTACT_ID, TAG_SYSTEMDATE
      from EMR_HPSITE_HX_DIAGNOSIS
      $timeRangeCondition
      order by HXDIAGNOSIS_ID, TAG_REVISION DESC
    """, "hxTmp")

  smartQueryExecutor.smartExecute("""
      select
        HXDIAGNOSIS_ID,
        first(TERM_UID) as TERM_UID,
        first(TAG_REVISION) as TAG_REVISION,
        first(DESCRIPTION) as DESCRIPTION,
        first(TITLE) as TITLE,
        first(PATIENT_ID) as PATIENT_ID,
        first(COMMENTS) as COMMENTS,
        first(DIAGNOSIS_ID) as DIAGNOSIS_ID,
        first(ONSET_DATE) as ONSET_DATE,
        first(CATEGORY_CODE) as CATEGORY_CODE,
        first(ACTIVITY_CODE) as ACTIVITY_CODE,
        first(MODIFIER_CODE) as MODIFIER_CODE,
        first(SIGNIFICANCE_CODE) as SIGNIFICANCE_CODE,
        first(INTENSITY_CODE) as INTENSITY_CODE,
        first(CONTACT_ID) as CONTACT_ID,
        first(TAG_SYSTEMDATE) as TAG_SYSTEMDATE
      from hxTmp
      group by HXDIAGNOSIS_ID
    """, "dfHx")

  val dfHxProblem = smartQueryExecutor.smartExecute(s"""
      select
        0 as DeleteInd,
        0 as TenantID,
        enc.ENC_CHARTPULLTIME as Version,
        'v$version' as CodeVersion,
        dx.HXDIAGNOSIS_ID as ProblemID,
        '' as ProblemCodeID,
        '' as ProblemCodeSystemID,
        coalesce(dx.DESCRIPTION, dx.TITLE) as ProblemDisplay,
        dx.COMMENTS as Comment,
        contact.IMRECONTACT_CODE,
        contact.IMREENC_CODE as EncounterID,
        termAtt.TERM_DESCRIPTION,
        dx.ONSET_DATE as OnsetDate,
        dictMaster.DICT_DESCRIPTION as Category,
        dx.CATEGORY_CODE as StatusCodeID,
        '' as StatusCodeSystemID,
        dictMaster2.DICT_DESCRIPTION as StatusDisplay,
        dictMaster3.DICT_DESCRIPTION as Modifier,
        dictMaster4.DICT_DESCRIPTION as Significance,
        dictMaster5.DICT_DESCRIPTION as Intensity,
        '' as Acuity,
        '' as Stability,
        enc.IMREENC_CODE,
        dx.PATIENT_ID as PersonID,
        dx.TAG_SYSTEMDATE as StatusChangeDate,
        prov1.PROV_ID as ProviderID,
        prov1.PROV_ID as ProviderIdentifier,
        prov1.PROV_TYPE as ProviderIdentifierType,
        prov1.PROV_LASTNAME as ProviderLastName,
        prov1.PROV_FIRSTNAME as ProviderFirstname,
        prov1.PROV_MIDDLEINITIAL as ProviderMiddleName,
        TRIM(CONCAT(prov1.PROV_LASTNAME, ', ', prov1.PROV_FIRSTNAME, ' ', prov1.PROV_MIDDLEINITIAL)) as ProviderFullName
      from dfHx dx
      left join EMR_HPSITE_TERMINOLOGY_ATTRIBUTES termAtt on termAtt.TERM_UID = dx.TERM_UID
      LEFT JOIN EMR_hpsystem_DICTIONARIES_MASTER dictMaster on dx.CATEGORY_CODE = dictMaster.DICT_CODE and dictMaster.DICT_TYPE = 'PREFTYPE'
      LEFT JOIN EMR_hpsystem_DICTIONARIES_MASTER dictMaster2 on dx.ACTIVITY_CODE = dictMaster2.DICT_CODE and dictMaster2.DICT_TYPE = 'DXACT'
      LEFT JOIN EMR_hpsystem_DICTIONARIES_MASTER dictMaster3 on dx.MODIFIER_CODE = dictMaster3.DICT_CODE and dictMaster3.DICT_TYPE = 'DXMOD'
      LEFT JOIN EMR_hpsystem_DICTIONARIES_MASTER dictMaster4 on dx.SIGNIFICANCE_CODE = dictMaster4.DICT_CODE and dictMaster4.DICT_TYPE = 'DXSIG'
      LEFT JOIN EMR_hpsystem_DICTIONARIES_MASTER dictMaster5 on dx.INTENSITY_CODE = dictMaster5.DICT_CODE and dictMaster5.DICT_TYPE = 'DXINT'
      JOIN EMR_HPSITE_CONTACT contact on contact.IMRECONTACT_CODE = dx.CONTACT_ID
      LEFT JOIN EMR_HPSITE_PROVIDERS prov1 on contact.IMREPROV_CODE = prov1.IMREPROV_CODE
      JOIN EMR_HPSITE_ENCOUNTER enc on enc.IMREENC_CODE = contact.IMREENC_CODE
      JOIN EMR_HPSITE_DEMOGRAPHICS demo on enc.IMREDEMEC_CODE = demo.IMREDEM_CODE
      where dx.DIAGNOSIS_ID is null
        and dx.ACTIVITY_CODE not in (8,9)
        and dx.CATEGORY_CODE = 24
        and demo.DEM_LASTNAME <> 'test'
        $goldenUserCondition
      order by dx.HXDIAGNOSIS_ID, dx.TAG_REVISION DESC
    """,
    "dfHxProblem")

  val dfProblem = dfDxProblem.unionAll(dfHxProblem)
  val bodyTxt = dfProblem.map(x=>
      (x.getAs[Integer]("DeleteInd") ::
        x.getAs[Integer]("TenantID") ::
        x.getAs[Integer]("ProblemID") ::
        x.getAs[String]("Version") + "_" + x.getAs[String]("CodeVersion") ::
        x.getAs[Integer]("PersonID") ::
        x.getAs[Integer]("EncounterID") ::
        x.getAs[String]("ProblemCodeID") ::
        x.getAs[String]("ProblemCodeSystemID") ::
        x.getAs[String]("ProblemDisplay") ::
        x.getAs[String]("OnsetDate") ::
        x.getAs[Integer]("StatusCodeID") ::
        x.getAs[String]("StatusCodeSystemID") ::
        x.getAs[String]("StatusDisplay") ::
        x.getAs[String]("StatusChangeDate") ::
        x.getAs[String]("ProviderID") ::
        x.getAs[String]("ProviderIdentifier") ::
        x.getAs[Integer]("ProviderIdentifierType") ::
        x.getAs[String]("ProviderLastName") ::
        x.getAs[String]("ProviderFirstname") ::
        x.getAs[String]("ProviderMiddleName") ::
        x.getAs[String]("ProviderFullName") ::
        x.getAs[String]("Comment") :: HNil).toPipeCsv)

  val destPath = s"${ConfigurationSunriseChoc.outputFolder}/problem"

  val headers = List("DeleteInd","TenantID","ProblemID","Version","PersonID","EncounterID","ProblemCodeID",
    "ProblemCodeSystemID","ProblemDisplay","OnsetDate","StatusCodeID","StatusCodeSystemID","StatusDisplay",
    "StatusChangeDate","ProviderID","ProviderIdentifier","ProviderIdentifierType","ProviderLastName","ProviderFirstname",
    "ProviderMiddleName","ProviderFullName","Comment")

  saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destPath)

  smartQueryExecutor.end()

  val end: Long = System.currentTimeMillis
  val diff = end - beg
  logger.info(s"------------It took $diff ms to write Problem to $destPath")
}
