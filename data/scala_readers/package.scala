package com.hart.hartos.etl

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.util.{OptionalOps, TimeConversions}

package object sunrise extends TimeConversions with OptionalOps with ConfigurationSunriseChoc.implicits {
}
