package com.hart.hartos.etl.sunrise

import java.util.Date

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.EncounterAliasTables
import com.hart.hartos.etl.util.FileIOSupport
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SQLContext
import shapeless.HNil

object EncounterAliasReader extends BaseSqlApp("EncounterAliasReader") with ExtraFunctions with EncounterAliasTables {
  EncounterAliasReaderProcess.execute(sqlContext, version)

  // Shutdown app
  sc.stop()
}

object EncounterAliasReaderProcess extends FileIOSupport with LazyLogging {
  def execute(sqlContext: SQLContext, version: String): Unit = {
    logger.info(s"------------EncounterAliasReader begins at ${new Date}")
    val beg: Long = System.currentTimeMillis

    val timeRangeCondition =
      if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        ""
      else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND demo.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
      else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND demo.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
      else
        s"AND demo.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND demo.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    val goldenUserCondition =
      if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
        ""
      else {
        val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
        s"AND demo.DEM_EXTERNALID in ('$mrnLists')"
      }
    val query = s"""
    SELECT distinct
      enc.IMREENC_CODE as EncounterID,
      demo.DEM_EXTERNALID as Alias,
      '2.16.840.1.113883.3.4173' as AssigningAuthority,
      'MR' as TypeCodeID,
      '2.16.840.1.113883.4.642.1.30' as TypeCodeSystemID,
      'Medical record number' as TypeCodeDisplay
    FROM EMR_HPSITE_DEMOGRAPHICS demo
    JOIN EMR_HPSITE_ENCOUNTER enc on enc.IMREDEMEC_CODE = demo.IMREDEM_CODE
    LEFT JOIN EMR_HPSITE_SITES site on enc.IMRESITE_CODE = site.IMRESITE_CODE
    WHERE demo.DEM_EXTERNALID <> '' and enc.ENC_DIAGNOSES <> '' and demo.DEM_LASTNAME <> 'test'
     $goldenUserCondition $timeRangeCondition
  """
    val output = sqlContext.sql(query)
    val bodyTxt = output.map(x=>
      (x.getAs[Integer]("EncounterID") ::
        x.getAs[String]("Alias") ::
        x.getAs[String]("AssigningAuthority") ::
        x.getAs[String]("TypeCodeID") ::
        x.getAs[String]("TypeCodeSystemID") ::
        x.getAs[String]("TypeCodeDisplay") :: HNil).toPipeCsv)

    val destPath = s"${ConfigurationSunriseChoc.outputFolder}/encounter_alias"
    val headers = List("EncounterID","Alias","AssigningAuthority","TypeCodeID","TypeCodeSystemID","TypeCodeDisplay")

    saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destPath)

    val end: Long = System.currentTimeMillis
    val diff = end - beg
    logger.info(s"------------It took $diff ms to write EncounterAlias to $destPath. Finished at ${new Date()}")
  }
}
