import luigi
import requests
from subprocess import call
from subprocess import Popen

import xml.etree.ElementTree as ET
from requests.auth import HTTPBasicAuth

class GetLatestJar(luigi.Task):

    is_done = False

    # TODO Config cleanup
    maven_base_url = "http://maven.hartsolutions.com:8081/artifactory"
    artifact_base_path = "sbt-release-local/com/hart"
    artifact_name = "sparkparquetreader_2.11"
    artifact_path = "%s/%s"

    username = "admin"
    password = "cf2ecSe4HhZr"

    meta_url = "%s/%s/%s/maven-metadata.xml" % (maven_base_url, artifact_base_path, artifact_name)

    def get_latest_version(self):
        resp = requests.get(self.meta_url, auth=HTTPBasicAuth('admin', 'cf2ecSe4HhZr'))
        xml_string = resp.text
        tree = ET.ElementTree(ET.fromstring(xml_string))
        root = tree.getroot()
        latest = root.find("versioning/latest").text
        return latest

    def get_artifact_url(self):
        version = self.get_latest_version()
        url = artifact_url = "%s/%s/%s/%s/%s-%s-assembly.jar" % (
            self.maven_base_url,
            self.artifact_base_path,
            self.artifact_name,
            version,
            self.artifact_name,
            version)
        return url

    def run(self):
        # I don't like that i had to use curl instead of
        # requests.
        # TODO make this work with requests
        cmd = [
            "curl",
            "-L",
            "-o",
            "/tmp/job.jar",
            "-H",
            "Authorization:Basic YWRtaW46Y2YyZWNTZTRIaFpy",
            self.get_artifact_url()
        ]
        process = Popen(cmd)
        process.wait()
        self.is_done = True

        f = self.output().open('w')
        f.write(".")
        f.close()

    def output(self):
        return luigi.LocalTarget('/tmp/choc_data_pipeline/get_latest_jar.lock')

if __name__ == '__main__':
    luigi.run(["--local-scheduler"], main_task_cls=GetLatestJar)