package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait EmrHpsiteArchHxDiagnosis extends HasSqlContext {
  sqlContext.read.parquet(ConfigurationSunriseChoc.inputFolder + "/EMR/HPSITE/ARCH_HXDIAGNOSIS")
    .registerTempTable("EMR_HPSITE_ARCH_HXDIAGNOSIS")
}
