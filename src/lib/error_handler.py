import traceback

from lib.slack_helper import log_error

class ErrorHandler(object):

    task_id = None

    def on_failure(self, exception):
        traceback_string = traceback.format_exc()
        log_error("The following task failed: {0}.\nThe exception is bellow.")
        log_error(traceback_string)
        return "Runtime error:\n%s" % traceback_string


if __name__ == '__main__':
    eh = ErrorHandler()
    try:
        raise ValueError('general exceptions not caught by specific handling')
    except ValueError as e:
        eh.on_failure(e)