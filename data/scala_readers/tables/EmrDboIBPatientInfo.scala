package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait EmrDboIBPatientInfo extends HasSqlContext {
  sqlContext.read.parquet(ConfigurationSunriseChoc.inputFolder + "/EMR/dbo/IB_Patient_Info")
    .registerTempTable("EMR_dbo_IB_Patient_Info")
}
