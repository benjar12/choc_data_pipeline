package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait EmrDboIBEmergencyContactInfo extends HasSqlContext {
  sqlContext.read.parquet(ConfigurationSunriseChoc.inputFolder + "/EMR/dbo/IB_Emergency_Contact_Info")
    .registerTempTable("EMR_dbo_IB_Emergency_Contact_Info")
}
