package com.hart.hartos.etl.sunrise

import java.util.Date

import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.MedicationIngredientsTables
import com.hart.hartos.etl.util.FileIOSupport
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SQLContext
import shapeless.HNil

object MedicationIngredientsReader extends BaseSqlApp("MedicationIngredientsReader")
  with ExtraFunctions with MedicationIngredientsTables with LazyLogging{
  MedicationIngredientsReaderProcess.execute(sqlContext, version)

  // Shutdown app
  sc.stop()
}

object MedicationIngredientsReaderProcess extends FileIOSupport with LazyLogging {
  def execute(sqlContext: SQLContext, version: String): Unit = {
    logger.info(s"------------MedicationIngredientsReader begins at ${new Date}")
    val beg: Long = System.currentTimeMillis

    val timeRangeCondition =
      if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        ""
      else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND med.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
      else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND med.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
      else
        s"AND med.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND med.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    val goldenUserCondition =
      if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
        ""
      else {
        val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
        s"AND dem.DEM_EXTERNALID in ('$mrnLists')"
      }
    val query = s"""
    SELECT distinct
      med.IMREMED_CODE as MedicationID,
      med.MED_CODE as DrugCodeID,
      med.MED_SOURCE as DrugCodeSystemID,
      med.MED_TITLE as DrugDisplay,
      med.MED_DOSE as DoseStrengthAmount,
      '' as DoseStrengthUnitCodeID,
      '' as DoseStrengthUnitCodeSystemID,
      med.MED_QUANTITYUNIT as DoseStrengthUnitDisplay,
      '' as DoseVolumeAmount,
      '' as DoseVolumeUnitCodeID,
      '' as DoseVolumeUnitCodeSystemID,
      '' as DoseVolumeUnitDisplay,
      med.MED_QUANTITY as DoseQuantity,
      '' as DoseQuantityUnitCodeID,
      '' as DoseQuantityUnitCodeSystemID,
      med.MED_QUANTITYTEXT as DoseQuantityUnitDisplay
    FROM EMR_HPSITE_MEDICATIONS med
    JOIN EMR_HPSITE_DEMOGRAPHICS dem ON dem.IMREDEM_CODE = med.IMREDEMEC_CODE
    WHERE dem.DEM_LASTNAME <> 'test' $goldenUserCondition $timeRangeCondition
  """
    val output = sqlContext.sql(query)
    val bodyTxt = output.map(x=>
      (x.getAs[Integer]("MedicationID") ::
        x.getAs[String]("DrugCodeID") ::
        x.getAs[String]("DrugCodeSystemID") ::
        x.getAs[String]("DrugDisplay") ::
        x.getAs[String]("DoseStrengthAmount") ::
        x.getAs[String]("DoseStrengthUnitCodeID") ::
        x.getAs[String]("DoseStrengthUnitCodeSystemID") ::
        x.getAs[String]("DoseStrengthUnitDisplay") ::
        x.getAs[String]("DoseVolumeAmount") ::
        x.getAs[String]("DoseVolumeUnitCodeID") ::
        x.getAs[String]("DoseVolumeUnitCodeSystemID") ::
        x.getAs[String]("DoseVolumeUnitDisplay") ::
        x.getAs[String]("DoseQuantity") ::
        x.getAs[String]("DoseQuantityUnitCodeID") ::
        x.getAs[String]("DoseQuantityUnitCodeSystemID") ::
        x.getAs[String]("DoseQuantityUnitDisplay") :: HNil).toPipeCsv)

    val destPath = s"${ConfigurationSunriseChoc.outputFolder}/medication_ingredients"
    val headers = List("MedicationID","DrugCodeID","DrugCodeSystemID","DrugDisplay","DoseStrengthAmount","DoseStrengthUnitCodeID",
      "DoseStrengthUnitCodeSystemID","DoseStrengthUnitDisplay","DoseVolumeAmount","DoseVolumeUnitCodeID","DoseVolumeUnitCodeSystemID",
      "DoseVolumeUnitDisplay","DoseQuantity","DoseQuantityUnitCodeID","DoseQuantityUnitCodeSystemID","DoseQuantityUnitDisplay")

    saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destPath)

    val end: Long = System.currentTimeMillis
    val diff = end - beg
    logger.info(s"------------It took $diff ms to write Medication Ingredients to $destPath. Finished at ${new Date}")
  }
}
