import os
import luigi
import paramiko

from fetch_from_hadoop import FetchFromHadoop
from lib.task_with_complete_handler import TaskWithCompleteHandler

PRIVATEKEY = os.path.abspath("../etc/id_rsa")
user = "bucket"
server = "78702571-b484-4b87-b822-7b6c9421b07c.hrt.io"
port = 22

class FTPUpload(TaskWithCompleteHandler):

    base_path = luigi.Parameter()
    current_date = luigi.Parameter()

    def mkdir(self, sftp, path, ignore_existing=False):
        ''' Augments mkdir by adding an option to not fail if the folder exists  '''
        try:
            sftp.mkdir(path)
        except IOError:
            if ignore_existing:
                pass
            else:
                raise

    def make_date_path(self, sftp, date_path):
        arr = date_path.split("/")
        base = "/bucket"
        year = "{0}/{1}".format(base, arr[0])
        month = "{0}/{1}".format(year, arr[1])
        day = "{0}/{1}".format(month, arr[2])
        self.mkdir(sftp, year, True)
        self.mkdir(sftp, month, True)
        self.mkdir(sftp, day, True)


    def put_dir(self, sftp, source, target):
        ''' Uploads the contents of the source directory to the target path. The
            target directory needs to exists. All subdirectories in source are
            created under target.
        '''
        for item in os.listdir(source):
            if os.path.isfile(os.path.join(source, item)):
                sftp.put(os.path.join(source, item), '%s/%s' % (target, item))
            else:
                self.mkdir(sftp, '%s/%s' % (target, item), ignore_existing=True)
                self.put_dir(sftp, os.path.join(source, item), '%s/%s' % (target, item))

    def requires(self):
        return [FetchFromHadoop(base_path=self.base_path, current_date=self.current_date)]

    def runner(self):
        print("Running FTP Upload")

        input_dir = "/tmp/choc/{0}".format(self.current_date)
        output_dir = "/bucket/{0}".format(self.current_date)

        trans = paramiko.Transport((server, port))
        rsa_key = paramiko.RSAKey.from_private_key_file(PRIVATEKEY)
        trans.connect(username=user, pkey=rsa_key)
        sftp = paramiko.SFTPClient.from_transport(trans)
        self.make_date_path(sftp, self.current_date)
        self.put_dir(sftp, input_dir, output_dir)



if __name__ == '__main__':
    luigi.run(["--local-scheduler"], main_task_cls=FTPUpload)