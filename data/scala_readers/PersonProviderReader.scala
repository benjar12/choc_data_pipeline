package com.hart.hartos.etl.sunrise

import java.util.Date

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.PersonProviderTables
import com.hart.hartos.etl.util.FileIOSupport
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SQLContext
import shapeless.HNil

object PersonProviderReader extends BaseSqlApp("PersonProviderReader") with ExtraFunctions 
  with PersonProviderTables with LazyLogging{
  PersonProviderReaderProcess.execute(sqlContext, version)

  // Shutdown app
  sc.stop()
}

object PersonProviderReaderProcess extends FileIOSupport with LazyLogging {
  def execute(sqlContext: SQLContext, version: String): Unit = {
    logger.info(s"------------PersonProviderReader begins at ${new Date}")
    val beg: Long = System.currentTimeMillis

    val timeRangeCondition =
      if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        ""
      else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND p.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
      else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND p.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
      else
        s"AND p.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND p.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    val goldenUserCondition =
      if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
        ""
      else {
        val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
        s"AND d.DEM_EXTERNALID in ('$mrnLists')"
      }
    val query = s"""
    select distinct
        d.IMREDEM_CODE as PersonID,
        p.PROV_ID as ProviderID,
        coalesce(p.PROV_UPIN, p.NPI, p.PROV_DEAID) as ProviderIdentifier,
        case when p.NPI <> '' then 'NPI' when p.PROV_UPIN <> '' then 'UPIN' else 'DEA' end as ProviderIdentifierType,
        p.PROV_LASTNAME as ProviderLastName,
        p.PROV_FIRSTNAME as ProviderFirstName,
        p.PROV_MIDDLEINITIAL as ProviderMiddleName,
        p.PROV_LASTNAME + ', ' + p.PROV_FIRSTNAME + (case when p.PROV_MIDDLEINITIAL != '' then ' ' + p.PROV_MIDDLEINITIAL else '' end) as ProviderFullName,
        'RP' as ProviderRoleCodeID,
        '2.16.840.1.133883.18.283' as ProviderRoleCodeSystemID,
        '' as ProviderRoleDisplay,
        '' as BeginDate,
        '' as EndDate
    from EMR_HPSITE_DEMOGRAPHICS as d
    join EMR_HPSITE_PROVIDERS as p on (p.IMREPROV_CODE = d.DEM_REFERRINGPROVIDER or p.IMREPROV_CODE = d.DEM_PREFERREDPROVIDER)
    WHERE d.DEM_LASTNAME <> 'test' $goldenUserCondition $timeRangeCondition
    """
    val output = sqlContext.sql(query)
    val bodyTxt = output.map(x=>
      (x.getAs[Integer]("PersonID") ::
        x.getAs[String]("ProviderID") ::
        x.getAs[String]("ProviderIdentifier") ::
        x.getAs[String]("ProviderIdentifierType") ::
        x.getAs[String]("ProviderLastName") ::
        x.getAs[String]("ProviderFirstName") ::
        x.getAs[String]("ProviderMiddleName") ::
        x.getAs[String]("ProviderFullName") ::
        x.getAs[String]("ProviderRoleCodeID") ::
        x.getAs[String]("ProviderRoleCodeSystemID") ::
        x.getAs[String]("ProviderRoleDisplay") ::
        x.getAs[String]("BeginDate") ::
        x.getAs[String]("EndDate") :: HNil).toPipeCsv)

    val destPath = s"${ConfigurationSunriseChoc.outputFolder}/person_provider"
    val headers = List("PersonID","ProviderID","ProviderIdentifier","ProviderIdentifierType","ProviderLastName",
      "ProviderFirstName","ProviderMiddleName","ProviderFullName","ProviderRoleCodeID","ProviderRoleCodeSystemID",
      "ProviderRoleDisplay","BeginDate","EndDate")

    saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destPath)

    val end: Long = System.currentTimeMillis
    val diff = end - beg
    logger.info(s"------------It took $diff ms to write Person Provider to $destPath. Finished at ${new Date()}")
  }
}
