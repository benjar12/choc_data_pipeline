package com.hart.hartos.etl.sunrise.tables

trait PersonAliasTables
  extends EmrHpsiteDemographics
  with EmrHpsiteProviders
  with LookupProvider
  with EmrHpsitePatientPhysicians {
}
