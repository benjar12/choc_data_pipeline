package com.hart.hartos.etl.sunrise

import java.util.Date

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.EncounterMedicalServiceTables
import com.hart.hartos.etl.util.FileIOSupport
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SQLContext
import shapeless.HNil

object EncounterBenefitCoverageReader extends BaseSqlApp("EncounterBenefitCoverageReader")
  with ExtraFunctions with EncounterMedicalServiceTables with LazyLogging {
  EncounterBenefitCoverageReaderProcess.execute(sqlContext, version)

  // Shutdown app
  sc.stop()
}

object EncounterBenefitCoverageReaderProcess extends FileIOSupport with LazyLogging {
  def execute(sqlContext: SQLContext, version: String): Unit = {
    logger.info(s"------------EncounterBenefitCoverageReader begins at ${new Date}")
    val beg: Long = System.currentTimeMillis

    val timeRangeCondition =
      if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        ""
      else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND demo.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
      else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND demo.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
      else
        s"AND demo.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND demo.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    val goldenUserCondition =
      if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
        ""
      else {
        val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
        s"AND demo.DEM_EXTERNALID in ('$mrnLists')"
      }
    sqlContext.sql("""
    SELECT demo.IMREDEM_CODE, INS_ENROLLMENTDATE, INS_TERMINATIONDATE, IMREINS_CODE
    FROM EMR_HPSITE_DEMOGRAPHICS demo
    JOIN EMR_HPSITE_ENCOUNTER enc on enc.IMREDEMEC_CODE = demo.IMREDEM_CODE
    JOIN EMR_HPSITE_INSURANCES ins on ins.IMREDEM_CODE = demo.IMREDEM_CODE
    WHERE enc.ENC_CHARTPULLTIME >= INS_ENROLLMENTDATE and
      (enc.ENC_CHARTPULLTIME <= INS_TERMINATIONDATE or INS_TERMINATIONDATE is null)
    order by INS_ENROLLMENTDATE desc, INS_TERMINATIONDATE desc
  """).registerTempTable("insCodes")

    sqlContext.sql("""
    SELECT IMREDEM_CODE, first(IMREINS_CODE) as IMREINS_CODE
    FROM insCodes
    GROUP BY IMREDEM_CODE
  """).registerTempTable("topCodes")

    val query = s"""
    SELECT distinct
      enc.IMREENC_CODE as EncounterID,
      '' as MemberID,
      case when ins.INS_PATREL_TOSUB = 0 then 'SEL' else 'UNK' end as RelationToSubscriberCodeID,
      '2.16.840.1.133883.18.24' as RelationToSubscriberCodeSystemID,
      '' as RelationToSubscriberDisplay,
      ins.INS_PLANNUMBER as SubscriberNumber,
      ins.INS_ENROLLMENTDATE as BeginDate,
      ins.INS_TERMINATIONDATE as EndDate,
      case when ins.INS_PLANTYPE = 0 then 'HMO' when ins.INS_PLANTYPE = 1 then 'PPO' else 'UNK' end as PlanIdentifier,
      '' as PlanIdentifierType,
      '' as PlanIdentifierAssigningAuthority,
      '' as Name,
      car.CARRIER_NAME as PayerName,
      ins.INS_GROUPNUMBER as PolicyOrGroupNumber,
      '' as PolicyOrGroupName,
      '' as LineOfBusiness,
      '' as BenefitTypeCodeID,
      '' as BenefitTypeCodeSystemID,
      '' as BenefitTypeDisplay,
      '' as PriorityRank,
      '' as ReferenceBenefitPlanID
    FROM EMR_HPSITE_DEMOGRAPHICS demo
    JOIN EMR_HPSITE_ENCOUNTER enc on enc.IMREDEMEC_CODE = demo.IMREDEM_CODE
    LEFT JOIN topCodes on topCodes.IMREDEM_CODE = demo.IMREDEM_CODE
    LEFT JOIN EMR_HPSITE_INSURANCES ins on ins.IMREINS_CODE = topCodes.IMREINS_CODE
    LEFT JOIN EMR_HPSITE_INSURANCECARRIERS car on car.IMRECARRIER_CODE = ins.IMRECARRIER_CODE
    WHERE enc.ENC_DIAGNOSES <> '' and demo.DEM_LASTNAME <> 'test'
      $goldenUserCondition $timeRangeCondition
    ORDER BY BeginDate DESC
  """
    val output = sqlContext.sql(query)
    val bodyTxt = output.map(x=>
      (x.getAs[Integer]("EncounterID") ::
        x.getAs[String]("MemberID") ::
        x.getAs[String]("RelationToSubscriberCodeID") ::
        x.getAs[String]("RelationToSubscriberCodeSystemID") ::
        x.getAs[String]("RelationToSubscriberDisplay") ::
        x.getAs[String]("SubscriberNumber") ::
        x.getAs[Date]("BeginDate") ::
        x.getAs[Date]("EndDate") ::
        x.getAs[String]("PlanIdentifier") ::
        x.getAs[String]("PlanIdentifierType") ::
        x.getAs[String]("PlanIdentifierAssigningAuthority") ::
        x.getAs[String]("Name") ::
        x.getAs[String]("PayerName") ::
        x.getAs[String]("PolicyOrGroupNumber") ::
        x.getAs[String]("PolicyOrGroupName") ::
        x.getAs[String]("LineOfBusiness") ::
        x.getAs[String]("BenefitTypeCodeID") ::
        x.getAs[String]("BenefitTypeCodeSystemID") ::
        x.getAs[String]("BenefitTypeDisplay") ::
        x.getAs[String]("PriorityRank") ::
        x.getAs[String]("ReferenceBenefitPlanID") :: HNil).toPipeCsv)

    val destPath = s"${ConfigurationSunriseChoc.outputFolder}/encounter_benefit_coverage"
    val headers = List("EncounterID","MemberID","RelationToSubscriberCodeID","RelationToSubscriberCodeSystemID",
      "RelationToSubscriberDisplay","SubscriberNumber","BeginDate","EndDate","PlanIdentifier","PlanIdentifierType",
      "PlanIdentifierAssigningAuthority","Name","PayerName","PolicyOrGroupNumber","PolicyOrGroupName","LineOfBusiness",
      "BenefitTypeCodeID","BenefitTypeCodeSystemID","BenefitTypeDisplay","PriorityRank","ReferenceBenefitPlanID")

    saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destPath)

    val end: Long = System.currentTimeMillis
    val diff = end - beg
    logger.info(s"------------It took $diff ms to write Encounter Benefit Coverage to $destPath. Finished at ${new Date()}")
  }
}