package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait MedispanMomdMomrte extends HasSqlContext {
  sqlContext.read.parquet(ConfigurationSunriseChoc.inputFolder + "/MEDISPAN/MOMD/MOMRTE")
    .registerTempTable("MEDISPAN_MOMD_MOMRTE")
}
