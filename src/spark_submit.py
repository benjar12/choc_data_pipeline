import os
import luigi
import luigi.contrib.hdfs

from get_latest_jar import GetLatestJar
from sqoop_required_tables import SqoopRequiredTables
from luigi.contrib.spark import SparkSubmitTask
from lib.spark_submit_sub_task import SparkSubmitSubTask

class SparkSubmit(SparkSubmitSubTask):

    print(os.environ)

    class_name = luigi.Parameter()
    output_postfix = luigi.Parameter()
    base_path = luigi.Parameter()
    current_date = luigi.Parameter()
    port = luigi.Parameter()

    driver_memory = '2g'
    executor_memory = '4g'
    num_executors = luigi.IntParameter(default=2)

    app = '/tmp/job.jar'
    entry_class_base = 'com.hart.hartos.etl.sunrise.{0}'

    output_folder = "/tmp/hartos/sparked/choc/sunrise/socpa"

    def requires(self):
        return [ GetLatestJar(), SqoopRequiredTables(base_path=self.base_path)]

    def app_options(self):
        # These are passed to the Spark main args in the defined order.
        return []

    def output(self):
        print(self.output_postfix)
        real_output = "{0}/{1}/{2}".format(self.output_folder, self.current_date, self.output_postfix)
        return luigi.contrib.hdfs.HdfsTarget(real_output, format=luigi.format.Text)