package com.hart.hartos.etl.sunrise

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.ImmunizationTables
import com.hart.hartos.etl.util.FileIOSupport
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import shapeless.HNil

object ImmunizationReader extends BaseSqlApp("ImmunizationReader") with ExtraFunctions with ImmunizationTables
  with FileIOSupport with LazyLogging {

  logger.info(s"------------ImmunizationReader v$version begins")
  val beg: Long = System.currentTimeMillis

  val timeRangeCondition =
    if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      ""
    else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
      s"AND imr.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      s"AND imr.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
    else
      s"AND imr.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND imr.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
  val goldenUserCondition =
    if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
      ""
    else {
      val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
      s"AND d.DEM_EXTERNALID in ('$mrnLists')"
    }
  val query = s"""
    SELECT distinct
      0 as DeleteInd,
      0 as TenantID,
      imr.IMMREC_ID as ImmunizationID,
      imr.IMMUNIZATION_ID,
      imr.TAG_SYSTEMDATE as Version,
      imr.ENTERED_DATE,
      'v$version' as CodeVersion,
      d.IMREDEM_CODE as PersonID,
      enc.IMREENC_CODE as EncounterID,
      imm.CPT_CODE as ImmunizationCodeID,
      '' as ImmunizationCodeSystemID,
      imm.IMMUNIZATION_NAME as ImmunizationDisplay,
      imr.GIVEN_DATE as ImmunizationDate,
      imr.DRUG_CODE as DrugCodeID,
      '' as DrugCodeSystemID,
      med.DRUG_TITLE as DrugDisplay,
      imr.MVX_CODE as DrugManufacturerCodeID,
      '' as DrugManufacturerCodeSystemID,
      cds.SHORT_NAME as DrugManufacturerDisplay,
      imr.LOT_NUMBER as DrugManufacturerLot,
      imm.DOSE_VALUE as DoseAmount,
      '' as DoseUnitCodeID,
      '' as DoseUnitCodeSystemID,
      imm.DOSE_UNITS as DoseUnitDisplay,
      imr.INJECTIONROUTE_CODE as RouteCodeID,
      '' as RouteCodeSystemID,
      case when imr.INJECTIONROUTE_CODE = 0 then ''
        when imr.INJECTIONROUTE_CODE = 1 then 'Intradermal'
        when imr.INJECTIONROUTE_CODE = 2 then 'Intramuscular'
        when imr.INJECTIONROUTE_CODE = 3 then 'Intranasal'
        when imr.INJECTIONROUTE_CODE = 4 then 'Oral'
        when imr.INJECTIONROUTE_CODE = 5 then 'Subcutaneous'
        else 'Other'
        end as RouteDisplay,
      imr.DO_NOT_PARTICIPATE_IN_VFC_FLAG as RefusalInd,
      imr.STATUS_CODE as StatusCodeID,
      '' as StatusCodeSystemID,
      case when imr.STATUS_CODE = 1 then 'Ordered'
        when imr.STATUS_CODE = 2 then 'Preliminary'
        when imr.STATUS_CODE = 4 then 'Complete'
        else ''
        end as StatusDisplay,
      imr.COMMENT as Comments
    FROM EMR_HPSITE_IMMUNIZATION_RECORD imr
    LEFT JOIN EMR_HPSITE_ENCOUNTER_ASSESSMENT enca ON imr.CONTACT_ID = enca.CONTACT_ID
    JOIN EMR_HPSITE_ENCOUNTER enc on enca.ENCOUNTER_ID = enc.IMREENC_CODE
    JOIN EMR_HPSITE_DEMOGRAPHICS d on enc.IMREDEMEC_CODE = d.IMREDEM_CODE
    LEFT JOIN EMR_HPSITE_IMMUNIZATIONS imm ON imr.IMMUNIZATION_ID = imm.IMMUNIZATION_ID
    LEFT JOIN MEDISPAN_MDDB_MEDNAME med ON imr.DRUG_CODE = med.DRUG_DESCRIPTOR_ID
    LEFT JOIN EMR_hpsystem_MVX_CODESET cds ON imr.MVX_CODE = cds.MVX_CODE
    WHERE d.DEM_LASTNAME <> 'test' $goldenUserCondition $timeRangeCondition
  """
  val output = sqlContext.sql(query)
  val bodyTxt = output.map(x=>
      (x.getAs[Integer]("DeleteInd") ::
        x.getAs[Integer]("TenantID") ::
        x.getAs[Integer]("ImmunizationID") ::
        x.getAs[String]("Version") + "_" + x.getAs[String]("CodeVersion") ::
        x.getAs[Integer]("PersonID") ::
        x.getAs[Integer]("EncounterID") ::
        x.getAs[String]("ImmunizationCodeID") ::
        x.getAs[String]("ImmunizationCodeSystemID") ::
        x.getAs[String]("ImmunizationDisplay") ::
        x.getAs[String]("ImmunizationDate") ::
        x.getAs[String]("DrugCodeID") ::
        x.getAs[String]("DrugCodeSystemID") ::
        x.getAs[String]("DrugDisplay") ::
        x.getAs[String]("DrugManufacturerCodeID") ::
        x.getAs[String]("DrugManufacturerCodeSystemID") ::
        x.getAs[String]("DrugManufacturerDisplay") ::
        x.getAs[String]("DrugManufacturerLot") ::
        x.getAs[Double]("DoseAmount") ::
        x.getAs[String]("DoseUnitCodeID") ::
        x.getAs[String]("DoseUnitCodeSystemID") ::
        x.getAs[String]("DoseUnitDisplay") ::
        x.getAs[Integer]("RouteCodeID") ::
        x.getAs[String]("RouteCodeSystemID") ::
        x.getAs[String]("RouteDisplay") ::
        x.getAs[Integer]("RefusalInd") ::
        x.getAs[Integer]("StatusCodeID") ::
        x.getAs[String]("StatusCodeSystemID") ::
        x.getAs[String]("StatusDisplay") ::
        x.getAs[String]("Comments") :: HNil).toPipeCsv)

  val destiPath = s"${ConfigurationSunriseChoc.outputFolder}/immunization"

  val headers = List("DeleteInd","TenantID","ImmunizationID","Version","PersonID","EncounterID","ImmunizationCodeID",
    "ImmunizationCodeSystemID","ImmunizationDisplay","ImmunizationDate","DrugCodeID","DrugCodeSystemID","DrugDisplay",
    "DrugManufacturerCodeID","DrugManufacturerCodeSystemID","DrugManufacturerDisplay","DrugManufacturerLot","DoseAmount",
    "DoseUnitCodeID","DoseUnitCodeSystemID","DoseUnitDisplay","RouteCodeID","RouteCodeSystemID","RouteDisplay","RefusalInd",
    "StatusCodeID","StatusCodeSystemID","StatusDisplay","Comments")

  saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destiPath)

  val end: Long = System.currentTimeMillis
  val diff = end - beg
  logger.info(s"------------It took $diff ms to write Immunization to $destiPath")
}
