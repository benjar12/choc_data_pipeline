package com.hart.hartos.etl.sunrise.tables

trait EncounterMedicalServiceTables
  extends EmrHpsiteDx
  with EmrHpsiteDemographics
  with EmrHpsiteInsurances
  with EmrHpsiteInsurancesCarriers
  with EmrHpsiteEncounter {
}
