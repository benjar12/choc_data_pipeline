package com.hart.hartos.etl.sunrise

import java.util.Date

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.EncounterProviderTables
import com.hart.hartos.etl.util.FileIOSupport
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SQLContext
import shapeless.HNil

object EncounterProviderReader extends BaseSqlApp("EncounterProviderReader") 
  with ExtraFunctions with EncounterProviderTables with LazyLogging {
  EncounterProviderReaderProcess.execute(sqlContext, version)

  // Shutdown app
  sc.stop()
}

object EncounterProviderReaderProcess extends FileIOSupport with LazyLogging {
  def execute(sqlContext: SQLContext, version: String): Unit = {
    logger.info(s"------------EncounterProviderReader begins at ${new Date}")
    val beg: Long = System.currentTimeMillis

    val timeRangeCondition =
      if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        ""
      else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND p.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
      else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND p.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
      else
        s"AND p.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND p.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    val goldenUserCondition =
      if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
        ""
      else {
        val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
        s"AND demo.DEM_EXTERNALID in ('$mrnLists')"
      }
    val query = s"""
    SELECT distinct
      e.IMREENC_CODE as EncounterID,
      p.PROV_ID as ProviderID,
      coalesce(p.PROV_UPIN, p.NPI, p.PROV_DEAID) as ProviderIdentifier,
      case when p.NPI <> '' then 'NPI' when p.PROV_UPIN <> '' then 'UPIN' else 'DEA' end as ProviderIdentifierType,
      p.PROV_LASTNAME as ProviderLastName,
      p.PROV_FIRSTNAME as ProviderFirstName,
      p.PROV_MIDDLEINITIAL as ProviderMiddleName,
      TRIM(CONCAT(p.PROV_LASTNAME, ', ', p.PROV_FIRSTNAME, ' ', p.PROV_MIDDLEINITIAL)) as ProviderFullName,
      'AD' as ProviderRoleCodeID,
      '2.16.840.1.133883.18.283' as ProviderRoleCodeSystemID,
      'Admitting' as ProviderRoleDisplay,
      e.ENC_CHARTPULLTIME as BeginDate,
      e.ENC_CHARTFILEDTIME as EndDate
    FROM EMR_HPSITE_ENCOUNTER e
    JOIN EMR_HPSITE_PROVIDERS p ON e.RECORDCAREGIVER_ID = p.IMREPROV_CODE
    JOIN EMR_HPSITE_DEMOGRAPHICS demo on e.IMREDEMEC_CODE = demo.IMREDEM_CODE
    WHERE e.ENC_DIAGNOSES <> '' and demo.DEM_LASTNAME <> 'test'
      $goldenUserCondition $timeRangeCondition
    """
    val output = sqlContext.sql(query)
    val bodyTxt = output.map(x=>
      (x.getAs[Integer]("EncounterID") ::
        x.getAs[String]("ProviderID") ::
        x.getAs[String]("ProviderIdentifier") ::
        x.getAs[String]("ProviderIdentifierType") ::
        x.getAs[String]("ProviderLastName") ::
        x.getAs[String]("ProviderFirstName") ::
        x.getAs[String]("ProviderMiddleName") ::
        x.getAs[String]("ProviderFullName") ::
        x.getAs[String]("ProviderRoleCodeID") ::
        x.getAs[String]("ProviderRoleCodeSystemID") ::
        x.getAs[String]("ProviderRoleDisplay") ::
        x.getAs[String]("BeginDate") ::
        x.getAs[String]("EndDate") :: HNil).toPipeCsv)

    val destPath = s"${ConfigurationSunriseChoc.outputFolder}/encounter_provider"
    val headers = List("EncounterID","ProviderID","ProviderIdentifier","ProviderIdentifierType","ProviderLastName",
      "ProviderFirstName","ProviderMiddleName","ProviderFullName","ProviderRoleCodeID","ProviderRoleCodeSystemID",
      "ProviderRoleDisplay","BeginDate","EndDate")

    saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destPath)

    val end: Long = System.currentTimeMillis
    val diff = end - beg
    logger.info(s"------------It took $diff ms to write Encounter Provider to $destPath. Finished at ${new Date}")
  }
}
