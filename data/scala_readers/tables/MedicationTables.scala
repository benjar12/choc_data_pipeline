package com.hart.hartos.etl.sunrise.tables

trait MedicationTables
  extends EmrHpsiteMedications
  with EmrHpsiteHxMedication
  with EmrHpsiteDemographics
  with EmrHpsiteContact
  with EmrHpsiteEncounter
  with EmrHpsiteEncounterAssessment
  with EmrHpsiteProviders {
}
