package com.hart.hartos.etl.sunrise

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.EncounterTables
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import org.apache.log4j.LogManager
import shapeless.HNil

object EncounterReader extends BaseSqlApp("EncounterReader")
  with ExtraFunctions with EncounterTables with LazyLogging{
  logger.info(s"------------EncounterReader v$version begins")
  val beg: Long = System.currentTimeMillis

  val timeRangeCondition =
    if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      ""
    else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
      s"AND demo.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      s"AND demo.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
    else
      s"AND demo.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND demo.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
  val goldenUserCondition =
    if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
      ""
    else {
      val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
      s"AND demo.DEM_EXTERNALID in ('$mrnLists')"
    }
  val query = s"""
    SELECT distinct
      0 as DeleteInd,
      0 as TenantID,
      enc.IMREENC_CODE as EncounterID,
      enc.ENC_CHARTPULLTIME as Version,
      'v$version' as CodeVersion,
      demo.IMREDEM_CODE as PersonID,
      enc.ENC_CHARTPULLTIME as ActualServiceDate,
      '' as EstimatedServiceDate,
      enc.ENC_CHARTFILEDTIME as DischargeDate,
      site.IMRESITE_CODE as FacilityID,
      site.SITE_NAME as FacilityDisplay,
      '' as EncounterNumber,
      '' as EncounterNumberAssigningAuthority,
      'O' as VisitTypeCodeID,
      '2.16.840.1.133883.18.5' as VisitTypeCodeSystemID,
      'HL7 v2 0004.json[VisitTypeCodeID]' as VisitTypeDisplay,
      '' as FinancialClassCodeID,
      '' as FinancialClassCodeSystemID,
      '' as FinancialClassDisplay,
      '' as AdmissionTypeCodeID,
      '' as AdmissionTypeCodeSystemID,
      '' as AdmissionTypeDisplay,
      '' as DischargeDispositionCodeID,
      '' as DischargeDispositionCodeSystemID,
      '' as DischargeDispositionDisplay,
      '' as DischargeToCodeID,
      '' as DischargeToCodeSystemID,
      '' as DischargeToDisplay,
      loc.LOCATION_ID as BuildingID,
      loc.LOCATION_NAME as BuildingDisplay,
      '' as NurseUnitID,
      '' as NurseUnitDisplay,
      rom.ROOM_ID as RoomID,
      rom.ROOM_TEXT as RoomDisplay,
      '' as BedID,
      '' as BedDisplay,
      '' as ServiceCodeID,
      '' as ServiceCodeSystemID,
      '' as ServiceDisplay,
      '' as AdmissionSourceCodeID,
      '' as AdmissionSourceCodeSystemID,
      '' as AdmissionSourceDisplay,
      '' as StatusCodeID,
      '' as StatusCodeSystemID,
      '' as StatusDisplay,
      '' as ReasonForVisitCodeID,
      '' as ReasonForVisitCodeSystemID,
      enc.ENC_DIAGNOSES as ReasonForVisitDisplay,
      '' as ServiceProviderOrgName,
      '' as ServiceProviderOrgID,
      '' as ServiceProviderOrgIdentifier,
      '' as ServiceProviderOrgIdentifierType
    FROM EMR_HPSITE_DEMOGRAPHICS demo
    JOIN EMR_HPSITE_ENCOUNTER enc on enc.IMREDEMEC_CODE = demo.IMREDEM_CODE 
    LEFT JOIN EMR_HPSITE_SITES site ON enc.IMRESITE_CODE = site.IMRESITE_CODE
    LEFT JOIN EMR_HPSITE_LOCATION loc on loc.LOCATION_ID = enc.LOCATION_ID
    LEFT JOIN EMR_HPSITE_LOCATION_ROOM rom ON loc.LOCATION_ID = rom.LOCATION_ID
    WHERE enc.ENC_DIAGNOSES <> '' and demo.DEM_LASTNAME <> 'test' $goldenUserCondition $timeRangeCondition
    ORDER BY ActualServiceDate desc
  """
  val output = sqlContext.sql(query)
  val bodyTxt = output.map(x=>
      (x.getAs[Integer]("DeleteInd") ::
        x.getAs[Integer]("TenantID") ::
        x.getAs[Integer]("EncounterID") ::
        x.getAs[String]("Version") + "_" + x.getAs[String]("CodeVersion") ::
        x.getAs[Integer]("PersonID") ::
        x.getAs[String]("ActualServiceDate") ::
        x.getAs[String]("EstimatedServiceDate") ::
        x.getAs[String]("DischargeDate") ::
        x.getAs[Integer]("FacilityID") ::
        x.getAs[String]("FacilityDisplay") ::
        x.getAs[String]("EncounterNumber") ::
        x.getAs[String]("EncounterNumberAssigningAuthority") ::
        x.getAs[String]("VisitTypeCodeID") ::
        x.getAs[String]("VisitTypeCodeSystemID") ::
        x.getAs[String]("VisitTypeDisplay") ::
        x.getAs[String]("FinancialClassCodeID") ::
        x.getAs[String]("FinancialClassCodeSystemID") ::
        x.getAs[String]("FinancialClassDisplay") ::
        x.getAs[String]("AdmissionTypeCodeID") ::
        x.getAs[String]("AdmissionTypeCodeSystemID") ::
        x.getAs[String]("AdmissionTypeDisplay") ::
        x.getAs[String]("DischargeDispositionCodeID") ::
        x.getAs[String]("DischargeDispositionCodeSystemID") ::
        x.getAs[String]("DischargeDispositionDisplay") ::
        x.getAs[String]("DischargeToCodeID") ::
        x.getAs[String]("DischargeToCodeSystemID") ::
        x.getAs[String]("DischargeToDisplay") ::
        x.getAs[Integer]("BuildingID") ::
        x.getAs[String]("BuildingDisplay") ::
        x.getAs[String]("NurseUnitID") ::
        x.getAs[String]("NurseUnitDisplay") ::
        x.getAs[Integer]("RoomID") ::
        x.getAs[String]("RoomDisplay") ::
        x.getAs[String]("BedID") ::
        x.getAs[String]("BedDisplay") ::
        x.getAs[String]("ServiceCodeID") ::
        x.getAs[String]("ServiceCodeSystemID") ::
        x.getAs[String]("ServiceDisplay") ::
        x.getAs[String]("AdmissionSourceCodeID") ::
        x.getAs[String]("AdmissionSourceCodeSystemID") ::
        x.getAs[String]("AdmissionSourceDisplay") ::
        x.getAs[String]("StatusCodeID") ::
        x.getAs[String]("StatusCodeSystemID") ::
        x.getAs[String]("StatusDisplay") ::
        x.getAs[String]("ReasonForVisitCodeID") ::
        x.getAs[String]("ReasonForVisitCodeSystemID") ::
        x.getAs[String]("ReasonForVisitDisplay") ::
        x.getAs[String]("ServiceProviderOrgName") ::
        x.getAs[String]("ServiceProviderOrgID") ::
        x.getAs[String]("ServiceProviderOrgIdentifier") ::
        x.getAs[String]("ServiceProviderOrgIdentifierType") :: HNil).toPipeCsv)
  val header = Array[String]("DeleteInd|TenantID|EncounterID|Version|PersonID|ActualServiceDate|EstimatedServiceDate|DischargeDate|FacilityID|FacilityDisplay|EncounterNumber|EncounterNumberAssigningAuthority|VisitTypeCodeID|VisitTypeCodeSystemID|VisitTypeDisplay|FinancialClassCodeID|FinancialClassCodeSystemID|FinancialClassDisplay|AdmissionTypeCodeID|AdmissionTypeCodeSystemID|AdmissionTypeDisplay|DischargeDispositionCodeID|DischargeDispositionCodeSystemID|DischargeDispositionDisplay|DischargeToCodeID|DischargeToCodeSystemID|DischargeToDisplay|BuildingID|BuildingDisplay|NurseUnitID|NurseUnitDisplay|RoomID|RoomDisplay|BedID|BedDisplay|ServiceCodeID|ServiceCodeSystemID|ServiceDisplay|AdmissionSourceCodeID|AdmissionSourceCodeSystemID|AdmissionSourceDisplay|StatusCodeID|StatusCodeSystemID|StatusDisplay|ReasonForVisitCodeID|ReasonForVisitCodeSystemID|ReasonForVisitDisplay|ServiceProviderOrgName|ServiceProviderOrgID|ServiceProviderOrgIdentifier|ServiceProviderOrgIdentifierType")
  val headerRDD = sc.parallelize(header)
  val outputTxt = headerRDD.union(bodyTxt)
  val destiPath = s"${ConfigurationSunriseChoc.outputFolder}/encounter"
  outputTxt.coalesce(1).saveAsTextFile(destiPath)
  val end: Long = System.currentTimeMillis
  val diff = end - beg
  logger.info(s"------------It took $diff ms to write Encounter to $destiPath")
}
