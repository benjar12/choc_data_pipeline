import luigi

from spark_submit import SparkSubmit

from lib.task_with_complete_handler import TaskWithCompleteHandler

class RunAllSparkJobs(TaskWithCompleteHandler):

    base_path = luigi.Parameter()
    current_date = luigi.Parameter()

    last_port = 5000

    jobs = [
        {"class": "AllergyReader",                  "output": "allergy"},
        {"class": "EncounterProviderReader",        "output": "encounter_provider"},
        {"class": "PersonProviderReader",           "output": "person_provider"},
        {"class": "DemographicReader",              "output": "demographic"},
        {"class": "EncounterReader",                "output": "encounter"},
        {"class": "ProblemReader",                  "output": "problem"},
        {"class": "DiagnosisReader",                "output": "diagnosis"},
        {"class": "ImmunizationReader",             "output": "immunization"},
        {"class": "ProceduresReader",               "output": "procedures"},
        {"class": "EncounterAliasReader",           "output": "encounter_alias"},
        {"class": "MedicationIngredientsReader",    "output": "medication_ingredients"},
        {"class": "ReferralRequestReader",          "output": "referral_request"},
        {"class": "EncounterBenefitCoverageReader", "output": "encounter_benefit_coverage"},
        {"class": "MedicationReader",               "output": "medication"},
        {"class": "ResultReader",                   "output": "result"},
        {"class": "EncounterLocationReader",        "output": "encounter_location"},
        {"class": "PersonAliasReader",              "output": "person_alias"},
        {"class": "EncounterMedicalServiceReader",  "output": "encounter_medical_service"},
        {"class": "PersonBenefitCoverageReader",    "output": "person_benefit_coverage"}
    ]

    def create_spark_submit_job(self, job):
        self.last_port += 1
        return SparkSubmit(
            class_name=job["class"],
            output_postfix=job["output"],
            base_path=self.base_path,
            current_date=self.current_date,
            port=self.last_port)

    def requires(self):
        to_be_ran = [self.create_spark_submit_job(job) for job in self.jobs]
        return to_be_ran