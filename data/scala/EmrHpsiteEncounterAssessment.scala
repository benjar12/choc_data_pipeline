package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait EmrHpsiteEncounterAssessment extends HasSqlContext {
  sqlContext.read.parquet(ConfigurationSunriseChoc.inputFolder + "/EMR/HPSITE/ENCOUNTER_ASSESSMENT")
    .registerTempTable("EMR_HPSITE_ENCOUNTER_ASSESSMENT")
}
