package com.hart.hartos.etl.sunrise

import java.sql.Date

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.DemographicTables
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.util.{FileIOSupport, SmartQueryExecutor}
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SQLContext
import shapeless.HNil

object DemographicReader extends BaseSqlApp("DemographicReader") with ExtraFunctions with DemographicTables
  with FileIOSupport with LazyLogging {
  DemographicReaderProcess.execute(sqlContext, version)

  // Shutdown App
  sc.stop()
}

object DemographicReaderProcess extends FileIOSupport with LazyLogging {
  def execute(sqlContext: SQLContext, version: String): Unit = {
    logger.info(s"------------DemographicReader v$version begins")
    val beg: Long = System.currentTimeMillis

    val timeRangeCondition =
      if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        ""
      else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND d.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
      else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND d.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
      else
        s"AND d.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND d.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    val goldenUserCondition =
      if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
        ""
      else {
        val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
        s"AND d.DEM_EXTERNALID in ('$mrnLists')"
      }
    val smartQueryExecutor = new SmartQueryExecutor(sqlContext, "choc_demographic")
    smartQueryExecutor.begin()

    smartQueryExecutor.smartExecute(
      s"""
      select IMREADDR_CODE, ADDR_ADDRESS, ADDR_CITY, ADDR_STATE, ADDR_ZIP, ADDR_PHONE, ADDR_EMAILADDRESS
      from EMR_HPSITE_ADDRESSES
      order by TAG_SYSTEMDATE desc
    """,
      "address_ordered")

    smartQueryExecutor.smartExecute(
      s"""
      select IMREADDR_CODE,
        first(ADDR_ADDRESS) as ADDR_ADDRESS,
        first(ADDR_CITY) as ADDR_CITY,
        first(ADDR_STATE) as ADDR_STATE,
        first(ADDR_ZIP) as ADDR_ZIP,
        first(ADDR_PHONE) as ADDR_PHONE,
        first(ADDR_EMAILADDRESS) as ADDR_EMAILADDRESS
      from address_ordered
      group by IMREADDR_CODE
    """,
      "address_ex")

    smartQueryExecutor.smartExecute(
      s"""
      select IMREDEM_CODE, PATCON_ADDRESS, PATCON_LASTNAME, PATCON_FIRSTNAME, PATCON_MIDDLENAME
      from EMR_HPSITE_PATIENT_CONTACTS
      where PATCON_EMERGENCY = 1
      order by PATCON_RELATIONSHIPCODE
    """,
      "contact_ordered")

    smartQueryExecutor.smartExecute(
      s"""
      select IMREDEM_CODE,
        first(PATCON_ADDRESS) as PATCON_ADDRESS,
        first(PATCON_LASTNAME) as PATCON_LASTNAME,
        first(PATCON_FIRSTNAME) as PATCON_FIRSTNAME,
        first(PATCON_MIDDLENAME) as PATCON_MIDDLENAME
      from contact_ordered
      group by IMREDEM_CODE
    """,
      "contact_ex")

    smartQueryExecutor.smartExecute(
      s"""
    SELECT distinct
      d.IMREDEM_CODE as PersonID,
      d.TAG_SYSTEMDATE as Version,
      'v$version' as CodeVersion,
      d.DEM_EXTERNALID as MRN,
      '2.16.840.1.113883.3.4173' as MRNAssigningAuthority,
      d.DEM_SSNUM as SSN,
      d.DEM_LASTNAME as LastName,
      d.DEM_FIRSTNAME as FirstName,
      d.DEM_MIDDLEINITIAL as MiddleName,
      d.DEM_DATEOFBIRTH as DOB,
      case when d.DEM_SEX = 1 then 'F' when d.DEM_SEX = 2 then 'M' else 'O' end as GenderCodeID,
      '2.16.840.1.133883.18.2' as GenderCodeSystemID,
      case when d.DEM_SEX = 1 then 'Female' when d.DEM_SEX = 2 then 'Male' else 'Other' end as GenderDisplay,
      case when r.RACE_CODE = 0 then '2106-3' else '2131-1' end as RaceCodeID1,
      '2.16.840.1.133883.18.6' as RaceCodeSystemID1,
      case when r.RACE_CODE = 0 then 'White' else 'Other Race' end as RaceDisplay1,
      '' as RaceCodeID2,
      '' as RaceCodeSystemID2,
      '' as RaceDisplay2,
      '' as RaceCodeID3,
      '' as RaceCodeSystemID3,
      '' as RaceDisplay3,
      case when e.ETHNICITY_CODE = 0 then 'N' when e.ETHNICITY_CODE = 1 then 'H' else 'U' end as EthnicityCodeID,
      '2.16.840.1.133883.18.100' as EthnicityCodeSystemID,
      '' as EthnicityDisplay,
      case when d.DEM_MARITALSTATUS = 1 then 'S' when d.DEM_MARITALSTATUS = 2 then 'M' else 'U' end as MaritalStatusCodeID,
      '2.16.840.1.133883.18.179' as MaritalStatusCodeSystemID,
      case when d.DEM_MARITALSTATUS = 1 then 'Single' when d.DEM_MARITALSTATUS = 2 then 'Married' else 'Unknown' end as MaritalStatusDisplay,
      '' as ReligionCodeID,
      '' as ReligionCodeSystemID,
      '' as ReligionDisplay,
      case when d.DEM_LANGUAGE = 0 then 'E' else 'U' end as PrimaryLanguageCodeID,
      '' as PrimaryLanguageCodeSystemID,
      case when d.DEM_LANGUAGE = 0 then 'English' else 'Unknown' end as PrimaryLanguageDisplay,
      a.ADDR_ADDRESS as StreetAddresss1,
      '' as StreetAddresss2,
      a.ADDR_CITY as City,
      TRIM(a.ADDR_STATE) as LocalityCodeID,
      '' as LocalityCodeSystemID,
      s1.STATE_NAME as LocalityDisplay,
      a.ADDR_ZIP as PostalCode,
      d.DEM_DECEASED as DeceasedInd,
      d.DEM_DATEOFDEATH as DeathDate,
      '' as CauseOfDeathCodeID,
      '' as CauseOfDeathCodeSystemID,
      '' as CauseOfDeathDisplay,
      a.ADDR_PHONE as PrimaryPhoneNumber,
      a.ADDR_EMAILADDRESS as EmailAddress,
      c.PATCON_LASTNAME as EmergencyContactLastName,
      c.PATCON_FIRSTNAME as EmergencyContactFirstName,
      c.PATCON_MIDDLENAME as EmergencyContactMiddleName,
      TRIM(CONCAT(c.PATCON_LASTNAME, ', ', c.PATCON_FIRSTNAME, ' ', c.PATCON_MIDDLENAME)) as EmergencyContactFullName,
      '' as EmergencyContactGenderCodeID,
      '' as EmergencyContactGenderCodeSystemID,
      '' as EmergencyContactGenderDisplay,
      ae.ADDR_ADDRESS as EmergencyContactStreetAddress1,
      '' as EmergencyContactStreetAddress2,
      ae.ADDR_CITY as EmergencyContactCity,
      TRIM(ae.ADDR_STATE) as EmergencyContactLocalityCodeID,
      '' as EmergencyContactLocalityCodeSystemID,
      s2.STATE_NAME as EmergencyContactLocalityDisplay,
      ae.ADDR_ZIP as EmergencyContactPostalCode,
      ae.ADDR_PHONE as EmergencyContactPrimaryPhoneNumber,
      ae.ADDR_EMAILADDRESS as EmergencyContactEmailAddress
    FROM EMR_HPSITE_DEMOGRAPHICS d
    LEFT JOIN EMR_HPSITE_ADDRESSES a on a.IMREADDR_CODE = d.DEM_HOMEADDR
    LEFT JOIN EMR_HPSITE_PATIENT_RACE r on r.PATIENT_ID = d.IMREDEM_CODE
    LEFT JOIN EMR_HPSITE_PATIENT_ETHNICITY e on e.PATIENT_ID = d.IMREDEM_CODE
    LEFT JOIN contact_ex c on c.IMREDEM_CODE = d.IMREDEM_CODE
    LEFT JOIN address_ex ae on ae.IMREADDR_CODE = c.PATCON_ADDRESS
    LEFT JOIN EMR_hpsystem_STATES s1 on TRIM(s1.STATE_CODE)=TRIM(a.ADDR_STATE)
    LEFT JOIN EMR_hpsystem_STATES s2 on TRIM(s2.STATE_CODE)=TRIM(ae.ADDR_STATE)
    WHERE d.DEM_LASTNAME <> 'test'
    $goldenUserCondition
    $timeRangeCondition
    """,
      "demographic")

    val output = sqlContext.sql("select * from demographic")
    val DeleteInd = 0
    val TenantId = 0

    val bodyTxt = output.map(x =>
      (
        DeleteInd ::
        TenantId ::
        x.getAs[Integer]("PersonID") ::
        x.getAs[String]("Version") + "_" + x.getAs[String]("CodeVersion") ::
        x.getAs[String]("MRN") ::
        x.getAs[String]("MRNAssigningAuthority") ::
        x.getAs[String]("SSN") ::
        x.getAs[String]("LastName") ::
        x.getAs[String]("FirstName") ::
        x.getAs[String]("MiddleName") ::
        x.getAs[Date]("DOB") ::
        x.getAs[String]("GenderCodeID") ::
        x.getAs[String]("GenderCodeSystemID") ::
        x.getAs[String]("GenderDisplay") ::
        x.getAs[String]("RaceCodeID1") ::
        x.getAs[String]("RaceCodeSystemID1") ::
        x.getAs[String]("RaceDisplay1") ::
        x.getAs[String]("RaceCodeID2") ::
        x.getAs[String]("RaceCodeSystemID2") ::
        x.getAs[String]("RaceDisplay2") ::
        x.getAs[String]("RaceCodeID3") ::
        x.getAs[String]("RaceCodeSystemID3") ::
        x.getAs[String]("RaceDisplay3") ::
        x.getAs[String]("EthnicityCodeID") ::
        x.getAs[String]("EthnicityCodeSystemID") ::
        x.getAs[String]("EthnicityDisplay") ::
        x.getAs[String]("MaritalStatusCodeID") ::
        x.getAs[String]("MaritalStatusCodeSystemID") ::
        x.getAs[String]("MaritalStatusDisplay") ::
        x.getAs[String]("ReligionCodeID") ::
        x.getAs[String]("ReligionCodeSystemID") ::
        x.getAs[String]("ReligionDisplay") ::
        x.getAs[String]("PrimaryLanguageCodeID") ::
        x.getAs[String]("PrimaryLanguageCodeSystemID") ::
        x.getAs[String]("PrimaryLanguageDisplay") ::
        x.getAs[String]("StreetAddresss1") ::
        x.getAs[String]("StreetAddresss2") ::
        x.getAs[String]("City") ::
        x.getAs[String]("LocalityCodeID") ::
        x.getAs[String]("LocalityCodeSystemID") ::
        x.getAs[String]("LocalityDisplay") ::
        x.getAs[String]("PostalCode") ::
        x.getAs[Integer]("DeceasedInd") ::
        x.getAs[Date]("DeathDate") ::
        x.getAs[String]("CauseOfDeathCodeID") ::
        x.getAs[String]("CauseOfDeathCodeSystemID") ::
        x.getAs[String]("CauseOfDeathDisplay") ::
        x.getAs[String]("PrimaryPhoneNumber") ::
        x.getAs[String]("EmailAddress") ::
        x.getAs[String]("EmergencyContactLastName") ::
        x.getAs[String]("EmergencyContactFirstName") ::
        x.getAs[String]("EmergencyContactMiddleName") ::
        x.getAs[String]("EmergencyContactFullName") ::
        x.getAs[String]("EmergencyContactGenderCodeID") ::
        x.getAs[String]("EmergencyContactGenderCodeSystemID") ::
        x.getAs[String]("EmergencyContactGenderDisplay") ::
        x.getAs[String]("EmergencyContactStreetAddress1") ::
        x.getAs[String]("EmergencyContactStreetAddress2") ::
        x.getAs[String]("EmergencyContactCity") ::
        x.getAs[String]("EmergencyContactLocalityCodeID") ::
        x.getAs[String]("EmergencyContactLocalityCodeSystemID") ::
        x.getAs[String]("EmergencyContactLocalityDisplay") ::
        x.getAs[String]("EmergencyContactPostalCode") ::
        x.getAs[String]("EmergencyContactPrimaryPhoneNumber") ::
        x.getAs[String]("EmergencyContactEmailAddress") :: HNil).toPipeCsv)

    val headers = List("DeleteInd", "TenantId", "PersonID", "Version", "MRN", "MRNAssigningAuthority", "SSN", "LastName", "FirstName", "MiddleName", "DOB",
      "GenderCodeID", "GenderCodeSystemID", "GenderDisplay", "RaceCodeID1", "RaceCodeSystemID1", "RaceDisplay1", "RaceCodeID2",
      "RaceCodeSystemID2", "RaceDisplay2", "RaceCodeID3", "RaceCodeSystemID3", "RaceDisplay3", "EthnicityCodeID", "EthnicityCodeSystemID",
      "EthnicityDisplay", "MaritalStatusCodeID", "MaritalStatusCodeSystemID", "MaritalStatusDisplay", "ReligionCodeID",
      "ReligionCodeSystemID", "ReligionDisplay", "PrimaryLanguageCodeID", "PrimaryLanguageCodeSystemID", "PrimaryLanguageDisplay",
      "StreetAddresss1", "StreetAddresss2", "City", "LocalityCodeID", "LocalityCodeSystemID", "LocalityDisplay", "PostalCode",
      "DeceasedInd", "DeathDate", "CauseOfDeathCodeID", "CauseOfDeathCodeSystemID", "CauseOfDeathDisplay", "PrimaryPhoneNumber",
      "EmailAddress", "EmergencyContactLastName", "EmergencyContactFirstName", "EmergencyContactMiddleName", "EmergencyContactFullName",
      "EmergencyContactGenderCodeID", "EmergencyContactGenderCodeSystemID", "EmergencyContactGenderDisplay", "EmergencyContactStreetAddress1",
      "EmergencyContactStreetAddress2", "EmergencyContactCity", "EmergencyContactLocalityCodeID", "EmergencyContactLocalityCodeSystemID",
      "EmergencyContactLocalityDisplay", "EmergencyContactPostalCode", "EmergencyContactPrimaryPhoneNumber", "EmergencyContactEmailAddress")

    val destiPath = s"${ConfigurationSunriseChoc.outputFolder}/demographic"

    saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destiPath)

    smartQueryExecutor.end()

    val end: Long = System.currentTimeMillis
    val diff = end - beg
    logger.info(s"------------It took $diff ms to write Demographic to $destiPath")
  }
}