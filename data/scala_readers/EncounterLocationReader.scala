package com.hart.hartos.etl.sunrise

import java.util.Date

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.EncounterLocationTables
import com.hart.hartos.etl.util.FileIOSupport
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SQLContext
import shapeless.HNil

object EncounterLocationReader extends BaseSqlApp("EncounterLocationReader")
  with ExtraFunctions with EncounterLocationTables with LazyLogging {
  EncounterLocationReaderProcess.execute(sqlContext, version)

  // Shutdown app
  sc.stop()
}

object EncounterLocationReaderProcess extends FileIOSupport with LazyLogging {
  def execute(sqlContext: SQLContext, version: String): Unit = {
    logger.info(s"------------EncounterLocationReader begins at ${new Date}")
    val beg: Long = System.currentTimeMillis

    val timeRangeCondition =
      if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        ""
      else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND enc.ENC_CHARTPULLTIME <'${ConfigurationSunriseChoc.endTime}'"
      else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND enc.ENC_CHARTPULLTIME >='${ConfigurationSunriseChoc.begTime}'"
      else
        s"AND enc.ENC_CHARTPULLTIME >='${ConfigurationSunriseChoc.begTime}' AND enc.ENC_CHARTPULLTIME <'${ConfigurationSunriseChoc.endTime}'"
    val goldenUserCondition =
      if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
        ""
      else {
        val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
        s"AND demo.DEM_EXTERNALID in ('$mrnLists')"
      }
    val query = s"""
    SELECT distinct
      enc.IMREENC_CODE as EncounterID,
      site.IMRESITE_CODE as FacilityID,
      site.SITE_NAME as FacilityDisplay,
      loc.LOCATION_ID as BuildingID,
      loc.LOCATION_NAME as BuildingDisplay,
      '' as NurseUnitID,
      '' as NurseUnitDisplay,
      rm.ROOM_ID as RoomID,
      rm.ROOM_TEXT as RoomDisplay,
      '' as BedID,
      '' as BedDisplay,
      enc.ENC_CHARTPULLTIME as BeginDate,
      enc.ENC_CHARTFILEDTIME as EndDate
    FROM EMR_HPSITE_ENCOUNTER enc
    JOIN EMR_HPSITE_DEMOGRAPHICS demo on enc.IMREDEMEC_CODE = demo.IMREDEM_CODE
    LEFT JOIN EMR_HPSITE_SITES site ON enc.IMRESITE_CODE = site.IMRESITE_CODE
    LEFT JOIN EMR_HPSITE_LOCATION loc ON enc.LOCATION_ID = loc.LOCATION_ID
    LEFT JOIN EMR_HPSITE_LOCATION_ROOM rm ON loc.LOCATION_ID = rm.LOCATION_ID
    WHERE enc.ENC_DIAGNOSES <> '' and demo.DEM_LASTNAME <> 'test'
      $goldenUserCondition $timeRangeCondition
    """
    val output = sqlContext.sql(query)
    val bodyTxt = output.map(x=>
      (x.getAs[Integer]("EncounterID") ::
        x.getAs[Integer]("FacilityID") ::
        x.getAs[String]("FacilityDisplay") ::
        x.getAs[Integer]("BuildingID") ::
        x.getAs[String]("BuildingDisplay") ::
        x.getAs[String]("NurseUnitID") ::
        x.getAs[String]("NurseUnitDisplay") ::
        x.getAs[Integer]("RoomID") ::
        x.getAs[String]("RoomDisplay") ::
        x.getAs[String]("BedID") ::
        x.getAs[String]("BedDisplay") ::
        x.getAs[String]("BeginDate") ::
        x.getAs[String]("EndDate") :: HNil).toPipeCsv)

    val destPath = s"${ConfigurationSunriseChoc.outputFolder}/encounter_location"
    val headers = List("EncounterID","FacilityID","FacilityDisplay","BuildingID","BuildingDisplay","NurseUnitID",
      "NurseUnitDisplay","RoomID","RoomDisplay","BedID","BedDisplay","BeginDate","EndDate")

    saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destPath)

    val end: Long = System.currentTimeMillis
    val diff = end - beg
    logger.info(s"------------It took $diff ms to write EncounterLocation to $destPath. Finished at ${new Date()}")
  }
}
