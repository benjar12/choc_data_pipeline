package com.hart.hartos.etl.sunrise

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.DiagnosisTables
import com.hart.hartos.etl.util.FileIOSupport
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SQLContext
import shapeless.HNil

object DiagnosisReader extends BaseSqlApp("DiagnosisReader") with ExtraFunctions with DiagnosisTables {
  DiagnosisReaderProcess.execute(sqlContext, version)

  // Shutdown app
  sc.stop()
}

object DiagnosisReaderProcess extends FileIOSupport with LazyLogging {
  def execute(sqlContext: SQLContext, version: String): Unit = {
    logger.info(s"------------DiagnosisReader v$version begins")
    val beg: Long = System.currentTimeMillis

    val timeRangeCondition =
      if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        ""
      else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND dm.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
      else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND dm.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
      else
        s"AND dm.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND dm.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    val goldenUserCondition =
      if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
        ""
      else {
        val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
        s"AND dm.DEM_EXTERNALID in ('$mrnLists')"
      }
    val query = s"""
    SELECT distinct
      dx.IMREDX_CODE as DiagnosisID,
      e.ENC_CHARTPULLTIME as Version,
      'v$version' as CodeVersion,
      dm.IMREDEM_CODE as PersonID,
      e.IMREENC_CODE as EncounterID,
      dx.DX_CODE as DiagnosisCodeID,
      '2.16.840.1.113883.6.103' as DiagnosisCodeSystemID,
      dx.DX_TITLE as DiagnosisDisplay,
      dx.TAG_SYSTEMDATE as DiagnosisDate,
      '' as ClassificationCodeID,
      '' as ClassificationCodeSystemID,
      '' as ClassificationDisplay,
      '' as ConfirmationCodeID,
      '' as ConfirmationCodeSystemID,
      '' as ConfirmationDisplay,
      p.IMREPROV_CODE as ProviderID,
      p.PROV_ID as ProviderIdentifier,
      p.PROV_TYPE as ProviderIdentifierType,
      p.PROV_LASTNAME as ProviderLastName,
      p.PROV_FIRSTNAME as ProviderFirstname,
      p.PROV_MIDDLEINITIAL as ProviderMiddleName,
      TRIM(CONCAT(p.PROV_LASTNAME, ', ', p.PROV_FIRSTNAME, ' ', p.PROV_MIDDLEINITIAL)) as ProviderFullName,
      dx.DX_COMMENT as Comment,
      e.ENC_BILLLEVEL as BillingRank,
      '' as PresentOnAdmissionCodeID,
      '' as PresentOnAdmissionCodeSystemID,
      '' as PresentOnAdmissionDisplay
    FROM EMR_HPSITE_DEMOGRAPHICS dm
    JOIN EMR_HPSITE_ENCOUNTER e on e.IMREDEMEC_CODE=dm.IMREDEM_CODE
    LEFT JOIN EMR_HPSITE_PROVIDERS p on dm.DEM_PREFERREDPROVIDER=p.IMREPROV_CODE
    LEFT JOIN EMR_HPSITE_ENCOUNTER_ASSESSMENT asmt ON e.IMREENC_CODE = asmt.ENCOUNTER_ID
    LEFT JOIN EMR_HPSITE_DX dx ON asmt.DX_ID = dx.IMREDX_CODE
  WHERE dx.DX_CODE <> '' and dm.DEM_LASTNAME <> 'test'
    $goldenUserCondition $timeRangeCondition
  """
    val output = sqlContext.sql(query)
    val DeleteInd = 0
    val TenantId = 0

    val bodyTxt = output.map(x=>
      (
        DeleteInd ::
        TenantId ::
        x.getAs[Integer]("DiagnosisID") ::
        x.getAs[String]("Version") + "_" + x.getAs[String]("CodeVersion") ::
        x.getAs[Integer]("PersonID") ::
        x.getAs[Integer]("EncounterID") ::
        x.getAs[String]("DiagnosisCodeID") ::
        x.getAs[String]("DiagnosisCodeSystemID") ::
        x.getAs[String]("DiagnosisDisplay") ::
        x.getAs[String]("DiagnosisDate") ::
        x.getAs[String]("ClassificationCodeID") ::
        x.getAs[String]("ClassificationCodeSystemID") ::
        x.getAs[String]("ClassificationDisplay") ::
        x.getAs[String]("ConfirmationCodeID") ::
        x.getAs[String]("ConfirmationCodeSystemID") ::
        x.getAs[String]("ConfirmationDisplay") ::
        x.getAs[Integer]("ProviderID") ::
        x.getAs[String]("ProviderIdentifier") ::
        x.getAs[Integer]("ProviderIdentifierType") ::
        x.getAs[String]("ProviderLastName") ::
        x.getAs[String]("ProviderFirstname") ::
        x.getAs[String]("ProviderMiddleName") ::
        x.getAs[String]("ProviderFullName") ::
        x.getAs[String]("Comment") ::
        x.getAs[String]("BillingRank") ::
        x.getAs[String]("PresentOnAdmissionCodeID") ::
        x.getAs[String]("PresentOnAdmissionCodeSystemID") ::
        x.getAs[String]("PresentOnAdmissionDisplay") :: HNil).toPipeCsv)

    val destPath = s"${ConfigurationSunriseChoc.outputFolder}/diagnosis"

    val headers = List("DeleteInd", "TenantId", "DiagnosisID","Version","PersonID","EncounterID","DiagnosisCodeID",
      "DiagnosisCodeSystemID","DiagnosisDisplay","DiagnosisDate","ClassificationCodeID","ClassificationCodeSystemID",
      "ClassificationDisplay","ConfirmationCodeID","ConfirmationCodeSystemID","ConfirmationDisplay","ProviderID",
      "ProviderIdentifier","ProviderIdentifierType","ProviderLastName","ProviderFirstname","ProviderMiddleName",
      "ProviderFullName","Comment","BillingRank","PresentOnAdmissionCodeID","PresentOnAdmissionCodeSystemID",
      "PresentOnAdmissionDisplay")

    saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destPath)

    val end: Long = System.currentTimeMillis
    val diff = end - beg
    logger.info(s"------------It took $diff ms to write Diagnosis to $destPath")
  }
}