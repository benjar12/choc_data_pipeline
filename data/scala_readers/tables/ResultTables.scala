package com.hart.hartos.etl.sunrise.tables

trait ResultTables
  extends EmrHpsiteLabResult
  with EmrHpsiteDemographics
  with EmrHpsiteEncounter
  with EmrHpsiteArchLabResult {
}
