package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait MedispanMddbMedname extends HasSqlContext {
  sqlContext.read.parquet(ConfigurationSunriseChoc.inputFolder + "/MEDISPAN/MDDB/MEDNAME")
    .registerTempTable("MEDISPAN_MDDB_MEDNAME")
}
