package com.hart.hartos.etl.sunrise.tables

trait ImmunizationTables
  extends EmrHpsiteImmunizationRecord
  with EmrHpsiteImmunizations
  with EmrHpsiteEncounterAssessment
  with MedispanMddbMedname
  with EmrHpsiteDemographics
  with EmrHpsiteEncounter
  with MedispanMomdMomrte
  with EmrHpsystemMvxCodeset {
}
