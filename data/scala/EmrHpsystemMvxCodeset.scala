package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait EmrHpsystemMvxCodeset extends HasSqlContext {
  sqlContext.read.parquet(ConfigurationSunriseChoc.inputFolder + "/EMR/hpsystem/MVX_CODESET")
    .registerTempTable("EMR_hpsystem_MVX_CODESET")
}
