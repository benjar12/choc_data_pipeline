package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait LookupRace extends HasSqlContext {
  sqlContext.read.json(ConfigurationSunriseChoc.hl7TableFolder + "/v2/0005.json")
    .registerTempTable("LOOKUP_RACE")
}
