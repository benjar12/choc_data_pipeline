package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait EmrHpsiteAllergenMappings extends HasSqlContext {
  sqlContext.read.parquet(ConfigurationSunriseChoc.inputFolder + "/EMR/HPSITE/Allergen_Mappings")
    .registerTempTable("EMR_HPSITE_Allergen_Mappings")
}
