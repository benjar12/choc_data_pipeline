package com.hart.hartos.etl.sunrise.tables

trait DiagnosisTables
  extends EmrHpsiteHxDiagnosis
  with EmrHpsiteDemographics
  with EmrHpsiteEncounter
  with EmrHpsiteArchHxDiagnosis
  with EmrHpsiteEncounterAssessment
  with EmrHpsiteDx
  with EmrHpsiteProviders {
}
