config = {
    'spark': {
        'classes': ['com.hart.SomeClassToRun']
    },

    # We will eventually set the artifact name to
    # to the spark parquet reader once it is in maven
    'maven': {
        'base_url': 'http://maven.hartsolutions.com:8081/artifactory',
        'artifact_base_path': 'libs-release-local/com/hart',
        'artifact_name': 'hl7parser_2.11',
        'username': '',
        'password': ''
    },

    # These are set to cobalt currently
    'hdfs': {
        'client_host': '2918b7bb-d13c-4041-ab9c-507da6592dd9.hrt.io',
        'base_path': 'hdfs://hart-cobalt'
    }
}