package com.hart.hartos.etl.sunrise

import java.util.Date

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.PersonAliasTables
import com.hart.hartos.etl.util.FileIOSupport
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SQLContext
import shapeless.HNil

object PersonAliasReader extends BaseSqlApp("PersonAliasReader") with ExtraFunctions 
  with PersonAliasTables {
  PersonAliasReaderProcess.execute(sqlContext, version)

  // Shutdown app
  sc.stop()
}

object PersonAliasReaderProcess extends FileIOSupport with LazyLogging {
  def execute(sqlContext: SQLContext, version: String): Unit = {
    logger.info(s"------------PersonAliasReader begins at ${new Date}")
    val beg: Long = System.currentTimeMillis

    val timeRangeCondition =
      if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        ""
      else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND d.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
      else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND d.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
      else
        s"AND d.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND d.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    val goldenUserCondition =
      if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
        ""
      else {
        val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
        s"AND d.DEM_EXTERNALID in ('$mrnLists')"
      }
    val query = s"""
    SELECT distinct
      d.IMREDEM_CODE as PersonID,
      d.DEM_EXTERNALID as Alias,
      '2.16.840.1.113883.3.4173' as AssigningAuthority,
      'MR' as TypeCodeID,
      '2.16.840.1.113883.4.642.1.30' as TypeCodeSystemID,
      'Medical record number' as TypeDisplay
    FROM EMR_HPSITE_DEMOGRAPHICS d
    WHERE d.DEM_EXTERNALID <> '' and d.DEM_LASTNAME <> 'test' $goldenUserCondition $timeRangeCondition
    """
    val output = sqlContext.sql(query)
    val bodyTxt = output.map(x=>
      (x.getAs[Integer]("PersonID") ::
        x.getAs[String]("Alias") ::
        x.getAs[String]("AssigningAuthority") ::
        x.getAs[String]("TypeCodeID") ::
        x.getAs[String]("TypeCodeSystemID") ::
        x.getAs[String]("TypeDisplay") :: HNil).toPipeCsv)

    val destPath = s"${ConfigurationSunriseChoc.outputFolder}/person_alias"
    val headers = List("PersonID","Alias","AssigningAuthority","TypeCodeID","TypeCodeSystemID","TypeDisplay")

    saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destPath)

    val end: Long = System.currentTimeMillis
    val diff = end - beg
    logger.info(s"------------It took $diff ms to write Person Alias to $destPath. Finished at ${new Date}")
  }
}
