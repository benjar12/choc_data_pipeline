package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait EmrHpsiteImmunizationRecord extends HasSqlContext {
  sqlContext.read.parquet(ConfigurationSunriseChoc.inputFolder + "/EMR/HPSITE/IMMUNIZATION_RECORD")
    .registerTempTable("EMR_HPSITE_IMMUNIZATION_RECORD")
}
