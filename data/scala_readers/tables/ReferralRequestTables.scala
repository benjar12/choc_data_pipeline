package com.hart.hartos.etl.sunrise.tables

trait ReferralRequestTables
  extends EmrHpsiteHxDiagnosis
  with EmrHpsiteArchHxDiagnosis
  with EmrHpsiteReferrals
  with EmrHpsiteEncounterAssessment
  with EmrHpsiteDx
  with EmrHpsystemStates
  with EmrHpsiteProviders
  with EmrHpsiteAddresses
  with EmrHpsiteEncounter
  with EmrHpsiteDemographics {
}
