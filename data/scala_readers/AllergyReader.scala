package com.hart.hartos.etl.sunrise

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.AllergyTables
import com.hart.hartos.etl.util.FileIOSupport
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SQLContext
import shapeless.HNil

object AllergyReader extends BaseSqlApp("AllergyReader") with ExtraFunctions with AllergyTables {
  AllergyReaderProcess.execute(sqlContext, version)

  // Shutdown app
  sc.stop()
}

object AllergyReaderProcess extends FileIOSupport with LazyLogging {
  def execute(sqlContext: SQLContext, version: String): Unit = {
    logger.info(s"------------AllergyReader v$version begins")
    val beg: Long = System.currentTimeMillis

    val timeRangeCondition =
      if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        ""
      else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND d.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
      else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND d.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
      else
        s"AND d.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND d.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    val goldenUserCondition =
      if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
        ""
      else {
        val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
        s"AND demo.DEM_EXTERNALID in ('$mrnLists')"
      }
    val query = s"""
    SELECT distinct
      0 as DeleteInd,
      0 as TenantID,
      d.HXDIAGNOSIS_ID as AllergyID,
      d.TAG_SYSTEMDATE as Version,
      'v$version' as CodeVersion,
      demo.IMREDEM_CODE as PersonID,
      enc.IMREENC_CODE as EncounterID,
      d.SOURCE_CODE as AllergenCodeID,
      d.SOURCE as AllergenCodeSystemID,
      d.TITLE as AllergenDisplay,
      d.ONSET_DATE as OnsetDate,
      '' as ReactionCodeID,
      '' as ReactionCodeSystemID,
      '' as ReactionDisplay,
      '' as SeverityCodeID,
      '' as SeverityCodeSystemID,
      '' as SeverityDisplay,
      d.COMMENTS as Comment,
      '' as StatusCodeID,
      '' as StatusCodeSystemID,
      '' as StatusDisplay,
      m.Common_Allergen_Id as CategoryCodeID,
      m.External_System as CategoryCodeSystemID,
      m.Common_Allergen as CategoryDisplay
    FROM EMR_HPSITE_HX_DIAGNOSIS d
    LEFT JOIN EMR_HPSITE_HX h on h.SOURCE_ID=d.HXDIAGNOSIS_ID
    JOIN EMR_HPSITE_ENCOUNTER enc on h.ENCOUNTER_ID = enc.IMREENC_CODE
    JOIN EMR_HPSITE_DEMOGRAPHICS demo on enc.IMREDEMEC_CODE = demo.IMREDEM_CODE
    LEFT JOIN EMR_HPSITE_Allergen_Mappings m on m.External_Code=d.SOURCE_CODE
    WHERE d.CATEGORY_CODE=21 and h.SOURCE = 'HX_DIAGNOSIS' and demo.DEM_LASTNAME <> 'test'
      $goldenUserCondition $timeRangeCondition
    """
    val output = sqlContext.sql(query)

    val bodyTxt = output.map(x=>
      (x.getAs[Integer]("DeleteInd") ::
        x.getAs[Integer]("TenantID") ::
        x.getAs[Integer]("AllergyID") ::
        x.getAs[String]("Version") + "_" + x.getAs[String]("CodeVersion") ::
        x.getAs[Integer]("PersonID") ::
        x.getAs[Integer]("EncounterID") ::
        x.getAs[String]("AllergenCodeID") ::
        x.getAs[String]("AllergenCodeSystemID") ::
        x.getAs[String]("AllergenDisplay") ::
        x.getAs[String]("OnsetDate") ::
        x.getAs[String]("ReactionCodeID") ::
        x.getAs[String]("ReactionCodeSystemID") ::
        x.getAs[String]("ReactionDisplay") ::
        x.getAs[String]("SeverityCodeID") ::
        x.getAs[String]("SeverityCodeSystemID") ::
        x.getAs[String]("SeverityDisplay") ::
        x.getAs[String]("Comment") ::
        x.getAs[String]("StatusCodeID") ::
        x.getAs[String]("StatusCodeSystemID") ::
        x.getAs[String]("StatusDisplay") ::
        x.getAs[String]("CategoryCodeID") ::
        x.getAs[String]("CategoryCodeSystemID") ::
        x.getAs[String]("CategoryDisplay") :: HNil).toPipeCsv)
    val destPath = s"${ConfigurationSunriseChoc.outputFolder}/allergy"

    val headers = List("DeleteInd","TenantID","AllergyID","Version","PersonID","EncounterID","AllergenCodeID",
      "AllergenCodeSystemID","AllergenDisplay","OnsetDate","ReactionCodeID","ReactionCodeSystemID","ReactionDisplay",
      "SeverityCodeID","SeverityCodeSystemID","SeverityDisplay","Comment","StatusCodeID","StatusCodeSystemID","StatusDisplay",
      "CategoryCodeID","CategoryCodeSystemID","CategoryDisplay")

    saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destPath)

    val end: Long = System.currentTimeMillis
    val diff = end - beg
    logger.info(s"------------It took $diff ms to write Allergy to $destPath")
  }
}
