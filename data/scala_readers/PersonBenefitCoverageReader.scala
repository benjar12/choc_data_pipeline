package com.hart.hartos.etl.sunrise


import java.util.Date

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.PersonBenefitCoverageTables
import com.hart.hartos.etl.util.FileIOSupport
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SQLContext
import shapeless.HNil

object PersonBenefitCoverageReader extends BaseSqlApp("PersonBenefitCoverageReader") with ExtraFunctions
  with PersonBenefitCoverageTables {
  PersonBenefitCoverageReaderProcess.execute(sqlContext, version)

  // Shutdown app
  sc.stop()
}

object PersonBenefitCoverageReaderProcess extends FileIOSupport with LazyLogging {
  def execute(sqlContext: SQLContext, version: String): Unit = {
    logger.info(s"------------PersonBenefitCoverageReader begins at ${new Date}")
    val beg: Long = System.currentTimeMillis

    val timeRangeCondition =
      if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        ""
      else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND d.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
      else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND d.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
      else
        s"AND d.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND d.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    val goldenUserCondition =
      if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
        ""
      else {
        val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
        s"and d.DEM_EXTERNALID in ('$mrnLists')"
      }
    val query = s"""
    SELECT distinct
      d.IMREDEM_CODE as PersonID,
      '' as MemberID,
      CASE WHEN i.INS_PATREL_TOSUB = 0 THEN 'SEL' ELSE 'UNK' END as RelationToSubscriberCodeID,
      '2.16.840.1.133883.18.24' as RelationToSubscriberCodeSystemID,
      '' as RelationToSubscriberDisplay,
      i.INS_PLANNUMBER as SubscriberNumber,
      i.INS_ENROLLMENTDATE as BeginDate,
      i.INS_TERMINATIONDATE as EndDate,
      CASE WHEN i.INS_PLANTYPE = 0 THEN 'HMO' WHEN i.INS_PLANTYPE = 1 THEN 'PPO' ELSE 'UNK' END as PlanIdentifier,
      '' as PlanIdentifierType,
      '' as PlanIdentifierAssigningAuthority,
      '' as Name,
      c.CARRIER_NAME as PayerName,
      i.INS_GROUPNUMBER as PolicyOrGroupNumber,
      '' as PolicyOrGroupName,
      '' as LineOfBusiness,
      '' as BenefitTypeCodeID,
      '' as BenefitTypeCodeSystemID,
      '' as BenefitTypeDisplay,
      '' as ReferenceBenefitPlanID
    FROM EMR_HPSITE_DEMOGRAPHICS d
    JOIN EMR_HPSITE_INSURANCES i on i.IMREDEM_CODE = d.IMREDEM_CODE
    JOIN EMR_HPSITE_INSURANCECARRIERS c on c.IMRECARRIER_CODE = i.IMRECARRIER_CODE
    WHERE d.DEM_LASTNAME <> 'test' $goldenUserCondition $timeRangeCondition
    """
    val output = sqlContext.sql(query)
    val bodyTxt = output.map(x=>{
      val groupNumber = if (x.getAs[String]("PolicyOrGroupNumber") == "BLANK" ||
        x.getAs[String]("PolicyOrGroupNumber") == "NONE") "" else x.getAs[String]("PolicyOrGroupNumber")
      (x.getAs[Integer]("PersonID") ::
        x.getAs[String]("MemberID") ::
        x.getAs[String]("RelationToSubscriberCodeID") ::
        x.getAs[String]("RelationToSubscriberCodeSystemID") ::
        x.getAs[String]("RelationToSubscriberDisplay") ::
        x.getAs[String]("SubscriberNumber") ::
        x.getAs[Date]("BeginDate") ::
        x.getAs[Date]("EndDate") ::
        x.getAs[String]("PlanIdentifier") ::
        x.getAs[String]("PlanIdentifierType") ::
        x.getAs[String]("PlanIdentifierAssigningAuthority") ::
        x.getAs[String]("Name") ::
        x.getAs[String]("PayerName") ::
        groupNumber ::
        x.getAs[String]("PolicyOrGroupName") ::
        x.getAs[String]("LineOfBusiness") ::
        x.getAs[String]("BenefitTypeCodeID") ::
        x.getAs[String]("BenefitTypeCodeSystemID") ::
        x.getAs[String]("BenefitTypeDisplay") ::
        x.getAs[String]("ReferenceBenefitPlanID") :: HNil).toPipeCsv})

    val destPath = s"${ConfigurationSunriseChoc.outputFolder}/person_benefit_coverage"
    val headers = List("PersonID","MemberID","RelationToSubscriberCodeID","RelationToSubscriberCodeSystemID",
      "RelationToSubscriberDisplay","SubscriberNumber","BeginDate","EndDate","PlanIdentifier","PlanIdentifierType",
      "PlanIdentifierAssigningAuthority","Name","PayerName","PolicyOrGroupNumber","PolicyOrGroupName","LineOfBusiness",
      "BenefitTypeCodeID","BenefitTypeCodeSystemID","BenefitTypeDisplay","ReferenceBenefitPlanID")

    saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destPath)

    val end: Long = System.currentTimeMillis
    val diff = end - beg
    logger.info(s"------------It took $diff ms to write Person Benefit Coverage to $destPath. Finished at ${new Date()}")
  }
}
