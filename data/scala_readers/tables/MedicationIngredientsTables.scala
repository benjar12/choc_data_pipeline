package com.hart.hartos.etl.sunrise.tables

trait MedicationIngredientsTables
  extends EmrHpsiteMedications
  with EmrHpsiteDemographics
  with EmrHpsiteProviders {
}
