package com.hart.hartos.etl.sunrise.tables

trait EncounterLocationTables
  extends EmrHpsiteLocation
  with EmrHpsiteLocationRoom
  with EmrHpsiteEncounter
  with EmrHpsiteDemographics
  with EmrHpsiteSites {
}
