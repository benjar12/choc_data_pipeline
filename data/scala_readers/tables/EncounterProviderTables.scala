package com.hart.hartos.etl.sunrise.tables

trait EncounterProviderTables
  extends EmrHpsiteEncounter
  with EmrHpsiteProviders
  with LookupProvider
  with EmrHpsiteDemographics
  with EmrHpsitePatientPhysicians {
}
