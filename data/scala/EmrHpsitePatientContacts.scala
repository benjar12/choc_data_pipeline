package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait EmrHpsitePatientContacts extends HasSqlContext {
  sqlContext.read.parquet(ConfigurationSunriseChoc.inputFolder + "/EMR/HPSITE/PATIENT_CONTACTS")
    .registerTempTable("EMR_HPSITE_PATIENT_CONTACTS")
}
