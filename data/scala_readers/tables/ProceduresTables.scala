package com.hart.hartos.etl.sunrise.tables

trait ProceduresTables
  extends EmrHpsiteProcedures
  with EmrHpsiteDemographics
  with EmrHpsiteEncounter
  with EmrHpsiteProviders {
}
