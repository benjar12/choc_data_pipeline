import luigi

from lib.external_program import ExternalProgramTask
from run_all_spark_jobs import RunAllSparkJobs

class MakeLocalDir(ExternalProgramTask):

    dir = luigi.Parameter()

    def program_args(self):
        cmd = [
            "mkdir",
            "-p",
            self.dir
        ]
        return cmd