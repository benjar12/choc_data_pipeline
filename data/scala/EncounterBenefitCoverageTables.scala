package com.hart.hartos.etl.sunrise.tables

trait EncounterBenefitCoverageTables
  extends EmrHpsiteDemographics
  with EmrHpsiteInsurances
  with EmrHpsiteInsurancesCarriers {
}
