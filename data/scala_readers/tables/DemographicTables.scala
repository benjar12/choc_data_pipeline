package com.hart.hartos.etl.sunrise.tables

trait DemographicTables
  extends EmrHpsiteDemographics
  with LookupGender
  with EmrHpsystemStates
  with EmrHpsiteAddresses
  with EmrHpsitePatientContacts
  with EmrHpsitePatientEthnicity
  with EmrHpsitePatientRace
  with EmrDboIBEmergencyContactInfo {
}
