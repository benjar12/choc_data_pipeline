package com.hart.hartos.etl.sunrise

import java.util.Date

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.EncounterMedicalServiceTables
import com.hart.hartos.etl.util.FileIOSupport
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SQLContext
import shapeless.HNil

object EncounterMedicalServiceReader extends BaseSqlApp("EncounterMedicalServiceReader")
  with ExtraFunctions with EncounterMedicalServiceTables with LazyLogging {
  EncounterMedicalServiceReaderProcess.execute(sqlContext, version)

  // Shutdown app
  sc.stop()
}

object EncounterMedicalServiceReaderProcess extends FileIOSupport with LazyLogging {
  def execute(sqlContext: SQLContext, version: String): Unit = {
    logger.info(s"------------EncounterMedicalServiceReader begins at ${new Date}")
    val beg: Long = System.currentTimeMillis

    val timeRangeCondition =
      if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        ""
      else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND ENC_CHARTPULLTIME <'${ConfigurationSunriseChoc.endTime}'"
      else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
        s"AND ENC_CHARTPULLTIME >='${ConfigurationSunriseChoc.begTime}'"
      else
        s"AND ENC_CHARTPULLTIME >='${ConfigurationSunriseChoc.begTime}' AND ENC_CHARTPULLTIME <'${ConfigurationSunriseChoc.endTime}'"
    val goldenUserCondition =
      if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
        ""
      else {
        val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
        s"AND demo.DEM_EXTERNALID in ('$mrnLists')"
      }
    val query = s"""
    SELECT distinct
      IMREENC_CODE as EncounterID,
      ENC_BILLLEVEL as ServiceCodeID,
      '2.16.840.1.113883.6.12' as ServiceCodeSystemID,
      '' as ServiceDisplay,
      ENC_CHARTPULLTIME as BeginDate,
      ENC_CHARTFILEDTIME as EndDate
    FROM EMR_HPSITE_ENCOUNTER e
    JOIN EMR_HPSITE_DEMOGRAPHICS demo on e.IMREDEMEC_CODE = demo.IMREDEM_CODE
    WHERE ENC_BILLLEVEL <> '' and ENC_DIAGNOSES <> '' and demo.DEM_LASTNAME <> 'test'
      $goldenUserCondition $timeRangeCondition
  """
    val output = sqlContext.sql(query)
    val bodyTxt = output.map(x=>
      (x.getAs[Integer]("EncounterID") ::
        x.getAs[String]("ServiceCodeID") ::
        x.getAs[String]("ServiceCodeSystemID") ::
        x.getAs[String]("ServiceDisplay") ::
        x.getAs[String]("BeginDate") ::
        x.getAs[String]("EndDate") :: HNil).toPipeCsv)

    val destPath = s"${ConfigurationSunriseChoc.outputFolder}/encounter_medical_service"
    val headers = List("EncounterID","ServiceCodeID","ServiceCodeSystemID","ServiceDisplay","BeginDate","EndDate")

    saveRDDAsTextFileWithHeader(bodyTxt, headers, "|", destPath)

    val end: Long = System.currentTimeMillis
    val diff = end - beg
    logger.info(s"------------It took $diff ms to write EncounterMedicalService to $destPath. Finished at ${new Date()}")
  }
}
