package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait EmrHpsystemDictionariesMaster extends HasSqlContext {
  sqlContext.read.parquet(ConfigurationSunriseChoc.inputFolder + "/EMR/hpsystem/DICTIONARIES_MASTER")
    .registerTempTable("EMR_hpsystem_DICTIONARIES_MASTER")
}
