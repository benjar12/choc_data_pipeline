import os

import pandas as pd


csv_path = os.path.abspath("../../data/sqoop_columns.csv")

sqoop_columns = pd.read_csv(csv_path)

sqoop_columns['db'] = sqoop_columns["db"].map(lambda x: x.lower())
sqoop_columns['schema'] = sqoop_columns["schema"].map(lambda x: x.lower())

csv_path = os.path.abspath("../../data/required_tables.csv")

required_tables = pd.read_csv(csv_path)

required_tables['db_copy'] = required_tables['db']
required_tables['schema_copy'] = required_tables['schema']

required_tables['db'] = required_tables["db"].map(lambda x: x.lower())
required_tables['schema'] = required_tables["schema"].map(lambda x: x.lower())

joined = pd.merge(required_tables, sqoop_columns, on=['db', 'schema', 'table'])

joined = joined[['db_copy', 'schema_copy', 'table', 'columns', 'java_types']]

joined.columns = ['db', 'schema', 'table', 'columns', 'java_types']

print( joined.head() )

csv_path = os.path.abspath("../../data/required_tables_with_columns.csv")

joined.to_csv(csv_path)
