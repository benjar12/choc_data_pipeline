from luigi import Task
from luigi import LocalTarget
from lib.error_handler import ErrorHandler
import logging

logger = logging.getLogger('luigi-interface')

class TaskWithCompleteHandler(Task, ErrorHandler):

    def runner(self):
        logger.warning(
            "The run method was not implemented by task {0}"
                .format(self.task_id)
        )

    # This assumes the runner function is blocking.
    # This is not a good solution to blocking the chain but it works.
    def run(self):
        self.runner()
        f = self.output().open('w')
        f.write(".")
        f.close()

    def output(self):
        task_name = self.task_id
        output_file_name = "/tmp/choc_data_pipeline/{0}.lock".format(task_name)
        return LocalTarget(output_file_name)