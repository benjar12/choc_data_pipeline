package com.hart.hartos.etl.sunrise.tables

trait ProblemTables
  extends EmrHpsiteContact
  with EmrHpsiteEncounter
  with EmrHpsiteDemographics
  with EmrHpsiteHxDiagnosis
  with EmrHpsiteDx
  with EmrHpsiteDictionariesSite
  with EmrHpsiteProviders
  with EmrHpsiteSites
  with EmrHpsiteTerminologyAttributes
  with EmrHpsystemDictionariesMaster {
}
