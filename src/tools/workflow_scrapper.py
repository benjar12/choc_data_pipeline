import re
import os

from pathlib import Path

import pandas as pd

columns_regex = re.compile(r"""<arg>--columns<\/arg>\n<arg>(.+?)<\/arg>""")
java_types_regex = re.compile(r"""<arg>--map-column-java<\/arg>\n<arg>(.+?)<\/arg>""")

def get_columns(xml_string):
    results = columns_regex.search(xml_string)
    if results is not None:
        return results.group(1)

def get_java_types(xml_string):
    results = java_types_regex.search(xml_string)
    if results is not None:
        return results.group(1)

workflows_base_path = os.path.abspath("../../data/workflows/")

def recur_dirs(path, files=[]):
    isdir = os.path.isdir(path)
    if isdir:
        return [recur_dirs(path + "/" + folder) for folder in os.listdir(path)]
    else:
        return path

files = recur_dirs(workflows_base_path)

def flatten(S):
    if S == []:
        return S
    if isinstance(S[0], list):
        return flatten(S[0]) + flatten(S[1:])
    return S[:1] + flatten(S[1:])

flat_files = flatten(files)


def get_file_content(file_name):
    content = Path(file_name).read_text()
    return (file_name, content)

names_and_content = [get_file_content(name) for name in flat_files]

names_and_columns = [( x[0], get_columns(x[1]), get_java_types(x[1]) ) for x in names_and_content]

cleaned = [x for x in names_and_columns if x[1] != None]

def get_db_schema_and_table(tup):
    file_path = tup[0]
    columns = tup[1]
    java_types = tup[2]
    list = file_path.split("/")[-4:][:-1]
    db = list[0]
    schema = list[1]
    table = list[2]
    return (db, schema, table, columns, java_types)

final = [get_db_schema_and_table(x) for x in cleaned]

headers = ['db', 'schema', 'table', 'columns', 'java_types']

df = pd.DataFrame(final, columns=headers)

print( df.head() )

csv_path = os.path.abspath("../../data/sqoop_columns.csv")

df.to_csv(csv_path)

#print(flat_files[1].split("/")[-4:][:-1])

