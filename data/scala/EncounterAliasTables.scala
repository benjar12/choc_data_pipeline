package com.hart.hartos.etl.sunrise.tables

trait EncounterAliasTables
  extends EmrHpsiteDemographics
  with EmrHpsiteEncounter
  with EmrHpsiteSites {
}
