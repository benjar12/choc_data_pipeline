package com.hart.hartos.etl.sunrise

import java.sql.Date

import com.hart.hartos.etl.config.ConfigurationSunriseChoc
import com.hart.hartos.etl.sunrise.tables.MedicationTables
import com.hart.hartos.etl.util.PipeCsvSupport._
import com.hart.hartos.etl.{BaseSqlApp, ExtraFunctions}
import com.typesafe.scalalogging.LazyLogging
import shapeless.HNil

object MedicationReader extends BaseSqlApp("MedicationReader") with ExtraFunctions 
  with MedicationTables with LazyLogging{
  logger.info(s"------------MedicationReader v$version begins")
  val beg: Long = System.currentTimeMillis

  val timeRangeCondition =
    if (ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      ""
    else if (ConfigurationSunriseChoc.begTime.isEmpty && !ConfigurationSunriseChoc.endTime.isEmpty)
      s"AND med.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
    else if (!ConfigurationSunriseChoc.begTime.isEmpty && ConfigurationSunriseChoc.endTime.isEmpty)
      s"AND med.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}'"
    else
      s"AND med.TAG_SYSTEMDATE >='${ConfigurationSunriseChoc.begTime}' AND med.TAG_SYSTEMDATE <'${ConfigurationSunriseChoc.endTime}'"
  val goldenUserCondition =
    if (ConfigurationSunriseChoc.goldenMRNs.isEmpty)
      ""
    else {
      val mrnLists = ConfigurationSunriseChoc.goldenMRNs.mkString("','")
      s"AND dem.DEM_EXTERNALID in ('$mrnLists')"
    }
  val query = s"""
    SELECT distinct
      0 as DeleteInd,
      0 as TenantID,
      med.IMREMED_CODE as MedicationID,
      med.TAG_SYSTEMDATE as Version,
      'v$version' as CodeVersion,
      dem.IMREDEM_CODE as PersonID,
      con.IMREENC_CODE as EncounterID,
      med.MED_STARTDATE as StartDate,
      med.MED_ENDDATE as StopDate,
      case when med.MED_STATUS = 1 then 'active'
        else 'stopped'
        end as OrderStatusCodeID,
      '2.16.840.1.113883.4.642.1.196' as OrderStatusCodeSystemID,
      case when med.MED_STATUS = 1 then 'Active'
        else 'Stopped'
        end as OrderStatusDisplay,
      med.MED_TITLE +
      (case when med.MED_DOSE <> '' then ' ' + med.MED_DOSE else '' end) +
      (case when med.MED_QUANTITYUNIT <> '' then ' ' + med.MED_QUANTITYUNIT  else '' end) +
      (case when med.MED_INTAKE <> '' then ', ' + med.MED_INTAKE else '' end) +
      (case when med.MED_DOSEUNIT <> '' then ' ' + med.MED_DOSEUNIT else '' end) +
      (case when med.MED_FREQUENCY <> '' then ' ' + med.MED_FREQUENCY else '' end) +
      (case when med.MED_QUANTITY <> '' then ', ' + med.MED_QUANTITY else '' end) +
      (case when med.MED_QUANTITYTEXT <> '' then ' ' +  med.MED_QUANTITYTEXT else '' end)
      as OrderDetailLine,
      prov.PROV_ID as PrescribingProviderID,
      case when prov.NPI <> '' then prov.NPI
              when prov.PROV_UPIN <> '' then prov.PROV_UPIN
              else prov.PROV_DEAID
              end as PrescribingProviderIdentifier,
       case when prov.NPI <> '' then 'NPI'
              when prov.PROV_UPIN <> '' then 'UPIN'
              else 'DEA'
              end as PrescribingProviderIdentifierType,
      prov.PROV_LASTNAME as PrescribingProviderLastName,
      prov.PROV_FIRSTNAME as PrescribingProviderFirstName,
      prov.PROV_MIDDLEINITIAL as PrescribingProviderMiddleName,
      prov.PROV_LASTNAME + ', ' + prov.PROV_FIRSTNAME + (case when prov.PROV_MIDDLEINITIAL != '' then ' ' + prov.PROV_MIDDLEINITIAL else '' end) as PrescribingProviderFullName,
      '' as RouteCodeID,
      '' as RouteCodeSystemID,
      med.MED_ROUTE as RouteDisplay,
      '' as FrequencyCodeID,
      '' as FrequencyCodeSystemID,
      CONCAT(med.MED_INTAKE, ' ', med.MED_DOSEUNIT, ' ', med.MED_FREQUENCY) as FrequencyDisplay,
      '' as PatientInstructions,
      med.MED_COMMENT as Comments,
      '' as IntendedAdministrator,
      '' as IntendedDispenser,
      med.MED_CODE as DrugCodeID,
      '2.16.840.1.113883.6.68' as DrugCodeSystemID,
      med.MED_TITLE as DrugDisplay,
      med.MED_DOSE as DoseStrengthAmount,
      '' as DoseStrengthUnitCodeID,
      '' as DoseStrengthUnitCodeSystemID,
      med.MED_QUANTITYUNIT as DoseStrengthUnitDisplay,
      '' as DoseVolumeAmount,
      '' as DoseVolumeUnitCodeID,
      '' as DoseVolumeUnitCodeSystemID,
      '' as DoseVolumeUnitDisplay,
      med.MED_QUANTITY as DoseQuantity,
      '' as DoseQuantityUnitCodeID,
      '' as DoseQuantityUnitCodeSystemID,
      med.MED_QUANTITYTEXT as DoseQuantityUnitDisplay
    FROM EMR_HPSITE_MEDICATIONS med
    JOIN EMR_HPSITE_DEMOGRAPHICS dem ON dem.IMREDEM_CODE = med.IMREDEMEC_CODE
    left join EMR_HPSITE_CONTACT con On con.IMRECONTACT_CODE = med.IMRECONTACT_CODE
    left join EMR_HPSITE_PROVIDERS prov On con.IMREPROV_CODE = prov.IMREPROV_CODE
    WHERE dem.DEM_LASTNAME <> 'test' $goldenUserCondition $timeRangeCondition
  """
  val output = sqlContext.sql(query)
  val bodyTxt = output.map(x=>
      (x.getAs[Integer]("DeleteInd") ::
        x.getAs[Integer]("TenantID") ::
        x.getAs[Integer]("MedicationID") ::
        x.getAs[String]("Version") + "_" + x.getAs[String]("CodeVersion") ::
        x.getAs[Integer]("PersonID") ::
        x.getAs[Integer]("EncounterID") ::
        x.getAs[Date]("StartDate") ::
        x.getAs[Date]("StopDate") ::
        x.getAs[String]("OrderStatusCodeID") ::
        x.getAs[String]("OrderStatusCodeSystemID") ::
        x.getAs[String]("OrderStatusDisplay") ::
        x.getAs[String]("OrderDetailLine") ::
        x.getAs[String]("PrescribingProviderID") ::
        x.getAs[String]("PrescribingProviderIdentifier") ::
        x.getAs[String]("PrescribingProviderIdentifierType") ::
        x.getAs[String]("PrescribingProviderLastName") ::
        x.getAs[String]("PrescribingProviderFirstName") ::
        x.getAs[String]("PrescribingProviderMiddleName") ::
        x.getAs[String]("PrescribingProviderFullName") ::
        x.getAs[String]("RouteCodeID") ::
        x.getAs[String]("RouteCodeSystemID") ::
        x.getAs[String]("RouteDisplay") ::
        x.getAs[String]("FrequencyCodeID") ::
        x.getAs[String]("FrequencyCodeSystemID") ::
        x.getAs[String]("FrequencyDisplay") ::
        x.getAs[String]("PatientInstructions") ::
        x.getAs[String]("Comments") ::
        x.getAs[String]("IntendedAdministrator") ::
        x.getAs[String]("IntendedDispenser") ::
        x.getAs[String]("DrugCodeID") ::
        x.getAs[String]("DrugCodeSystemID") ::
        x.getAs[String]("DrugDisplay") ::
        x.getAs[String]("DoseStrengthAmount") ::
        x.getAs[String]("DoseStrengthUnitCodeID") ::
        x.getAs[String]("DoseStrengthUnitCodeSystemID") ::
        x.getAs[String]("DoseStrengthUnitDisplay") ::
        x.getAs[String]("DoseVolumeAmount") ::
        x.getAs[String]("DoseVolumeUnitCodeID") ::
        x.getAs[String]("DoseVolumeUnitCodeSystemID") ::
        x.getAs[String]("DoseVolumeUnitDisplay") ::
        x.getAs[String]("DoseQuantity") ::
        x.getAs[String]("DoseQuantityUnitCodeID") ::
        x.getAs[String]("DoseQuantityUnitCodeSystemID") ::
        x.getAs[String]("DoseQuantityUnitDisplay") :: HNil).toPipeCsv)
  val header = Array[String]("DeleteInd|TenantID|MedicationID|Version|PersonID|EncounterID|StartDate|StopDate|OrderStatusCodeID|OrderStatusCodeSystemID|OrderStatusDisplay|OrderDetailLine|PrescribingProviderID|PrescribingProviderIdentifier|PrescribingProviderIdentifierType|PrescribingProviderLastName|PrescribingProviderFirstName|PrescribingProviderMiddleName|PrescribingProviderFullName|RouteCodeID|RouteCodeSystemID|RouteDisplay|FrequencyCodeID|FrequencyCodeSystemID|FrequencyDisplay|PatientInstructions|Comments|IntendedAdministrator|IntendedDispenser|DrugCodeID|DrugCodeSystemID|DrugDisplay|DoseStrengthAmount|DoseStrengthUnitCodeID|DoseStrengthUnitCodeSystemID|DoseStrengthUnitDisplay|DoseVolumeAmount|DoseVolumeUnitCodeID|DoseVolumeUnitCodeSystemID|DoseVolumeUnitDisplay|DoseQuantity|DoseQuantityUnitCodeID|DoseQuantityUnitCodeSystemID|DoseQuantityUnitDisplay")
  val headerRDD = sc.parallelize(header)
  val outputTxt = headerRDD.union(bodyTxt)
  val destiPath = s"${ConfigurationSunriseChoc.outputFolder}/medication"
  outputTxt.coalesce(1).saveAsTextFile(destiPath)
  val end: Long = System.currentTimeMillis
  val diff = end - beg
  logger.info(s"------------It took $diff ms to write Medication to $destiPath")
}
