import luigi

from datetime import datetime
from ftp_upload import FTPUpload

from lib.task_with_complete_handler import TaskWithCompleteHandler

class RunAndClean(TaskWithCompleteHandler):

    current_date = datetime.now().strftime("%Y/%m/%d")
    base_path = "/tmp/hartos/sqoop/choc/sunrise/socpa/{0}".format(current_date)

    def requires(self):
        return [FTPUpload(base_path=self.base_path, current_date=self.current_date)]

    def run(self):
        #TODO add logic to clean up after running
        print("Running")