package com.hart.hartos.etl.sunrise.tables

import com.hart.hartos.etl.HasSqlContext
import com.hart.hartos.etl.config.ConfigurationSunriseChoc

trait EmrHpsiteTerminologyAttributes extends HasSqlContext {
  sqlContext.read.parquet(ConfigurationSunriseChoc.inputFolder + "/EMR/HPSITE/TERMINOLOGY_ATTRIBUTES")
    .registerTempTable("EMR_HPSITE_TERMINOLOGY_ATTRIBUTES")
}
