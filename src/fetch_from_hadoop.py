import luigi

from lib.external_program import ExternalProgramTask
from run_all_spark_jobs import RunAllSparkJobs
from make_local_dir import MakeLocalDir

class FetchFromHadoop(ExternalProgramTask):

    base_path = luigi.Parameter()
    current_date = luigi.Parameter()

    def get_local_dir(self):
        return "/tmp/choc/{0}".format(self.current_date)

    def requires(self):
        return [
            RunAllSparkJobs(base_path=self.base_path, current_date=self.current_date),
            MakeLocalDir(dir=self.get_local_dir())
        ]

    def program_args(self):
        hdfs_path = "/tmp/hartos/sparked/choc/sunrise/socpa/{0}/*".format(self.current_date)
        local_output_dir = self.get_local_dir()
        cmd = [
            "hdfs",
            "dfs",
            "-get",
            #"-r",
            hdfs_path,
            local_output_dir
        ]
        return cmd
