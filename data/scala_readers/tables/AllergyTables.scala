package com.hart.hartos.etl.sunrise.tables

trait AllergyTables
  extends EmrHpsiteHxDiagnosis
  with EmrHpsiteDemographics
  with EmrHpsiteEncounter
  with EmrHpsiteHx
  with EmrHpsiteAllergenMappings
  with EmrHpsiteArchHxDiagnosis
  with EmrHpsiteProviders {
}
